package pjrpc

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"

	"gitlab.com/pjrpc/pjrpc/v2/pjson"
)

const (
	// ContentTypeHeaderName name of the header Content-Type.
	ContentTypeHeaderName = "Content-Type"
	// ContentTypeHeaderValue value of the header Content-Type.
	ContentTypeHeaderValue = "application/json"
	// JSONRPCVersion is a version of supported JSON-RPC protocol.
	JSONRPCVersion = "2.0"
)

// convertRawMessageToString trims " symbols from JSON RawMessage and returns value of the ID.
func convertRawMessageToString(m json.RawMessage) string {
	if m == nil {
		return ""
	}

	return string(bytes.Trim(m, `"`))
}

// Request model of the JSON-RPC request.
type Request struct {
	// A String specifying the version of the JSON-RPC protocol. MUST be exactly "2.0".
	JSONRPC string `json:"jsonrpc"`
	// An identifier established by the Client that MUST contain a String, Number, or NULL value if included.
	ID json.RawMessage `json:"id,omitempty"`
	// A String containing the name of the method to be invoked.
	Method string `json:"method"`
	// A Structured value that holds the parameter values to be used during the invocation of the method.
	Params json.RawMessage `json:"params,omitempty"`
}

// GetID returns id of the request as a string.
func (r *Request) GetID() string {
	return convertRawMessageToString(r.ID)
}

// JSON converts Request object to JSON message.
func (r *Request) JSON() json.RawMessage {
	res, _ := pjson.Marshal(r) //nolint:errcheck // You can't catch error with regular struct.
	return res
}

// IsNotification returns true when request doesn't have id.
func (r *Request) IsNotification() bool {
	return r.ID == nil
}

// NewRequest returns new Request object with marshaled ID and Params.
// You can pass ID as empty string if you want to create a Notification request.
func NewRequest(id, method string, params interface{}) (req *Request, err error) {
	req = &Request{
		JSONRPC: JSONRPCVersion,
		ID:      nil,
		Method:  method,
		Params:  nil,
	}

	if id != "" {
		req.ID, _ = pjson.Marshal(id) //nolint:errcheck // Impossible error from Marshal of string.
	}

	req.Params, err = pjson.Marshal(params)
	if err != nil {
		return nil, fmt.Errorf("pjson.Marshal params: %w", err)
	}

	return req, nil
}

// NewRequestFromJSON returns a new Request parsed from JSON.
func NewRequestFromJSON(msg json.RawMessage) (*Request, error) {
	req := Request{}

	err := pjson.Unmarshal(msg, &req)
	if err != nil {
		return nil, fmt.Errorf("pjson.Unmarshal: %w", err)
	}

	return &req, nil
}

// ErrorResponse model of the response with error.
type ErrorResponse struct {
	// A Number that indicates the error type that occurred.
	Code int `json:"code"`
	// A String providing a short description of the error.
	Message string `json:"message"`
	// A Primitive or Structured value that contains additional information about the error.
	Data json.RawMessage `json:"data,omitempty"`
}

// Error implementation error interface.
func (e *ErrorResponse) Error() string {
	if e == nil {
		return "<nil>"
	}

	tmpl := "JSON-RPC Error: [%d] %s"
	data := ""
	if len(e.Data) != 0 {
		tmpl += " (%s)"
		data = string(e.Data)
	} else {
		tmpl += "%s"
	}

	return fmt.Sprintf(tmpl, e.Code, e.Message, data)
}

// Response model of the response object.
type Response struct {
	// A String specifying the version of the JSON-RPC protocol. MUST be exactly "2.0".
	JSONRPC string `json:"jsonrpc"`
	// It MUST be the same as the value of the id member in the Request.
	ID json.RawMessage `json:"id,omitempty"`
	// This member is REQUIRED on success. The value of this member is determined
	// by the method invoked on the Server.
	Result json.RawMessage `json:"result,omitempty"`
	// This member is REQUIRED on error. This member MUST NOT exist if there was no error
	// triggered during invocation.
	Error *ErrorResponse `json:"error,omitempty"`
}

// NewResponseFromJSON parses json message and returns Response object.
func NewResponseFromJSON(msg json.RawMessage) (*Response, error) {
	resp := Response{}

	err := pjson.Unmarshal(msg, &resp)
	if err != nil {
		return nil, fmt.Errorf("pjson.Unmarshal: %w", err)
	}

	return &resp, nil
}

// NewResponseFromJSONReader reads io.Reader as a JSON data.
// Returns Response object and parse error.
func NewResponseFromJSONReader(r io.Reader) (*Response, error) {
	resp := Response{}

	err := pjson.NewDecoder(r).Decode(&resp)
	if err != nil {
		return nil, fmt.Errorf("decode json reader: %w", err)
	}

	return &resp, nil
}

// NewResponseFromError returns response model based on Error.
func NewResponseFromError(err error) *Response {
	resp := Response{JSONRPC: JSONRPCVersion}
	resp.SetError(err)

	return &resp
}

// NewResponseFromRequest returns new response model based on Request.
func NewResponseFromRequest(req *Request) *Response {
	return &Response{JSONRPC: JSONRPCVersion, ID: req.ID}
}

// UnmarshalResult parses Result field in the Response object to destination param.
func (r *Response) UnmarshalResult(dst interface{}) error {
	if r.Result == nil {
		return nil
	}

	return pjson.Unmarshal(r.Result, dst) //nolint:wrapcheck // No need to wrap the single error.
}

// GetID returns id of the response as a string.
func (r *Response) GetID() string {
	return convertRawMessageToString(r.ID)
}

// SetError sets error to Response model.
func (r *Response) SetError(err error) *Response {
	r.Error = convertError(err)
	r.Result = nil

	return r
}

// SetResult sets result to Response model.
func (r *Response) SetResult(result interface{}) *Response {
	resultJSON, err := pjson.Marshal(result)
	if err != nil {
		return r.SetError(JRPCErrInternalError("failed to marshal response"))
	}

	r.Result = resultJSON
	r.Error = nil

	return r
}

// JSON converts response model into JSON text.
func (r *Response) JSON() json.RawMessage {
	msg, _ := pjson.Marshal(r) //nolint:errcheck // You can't catch error with regular struct.
	return msg
}

// BatchRequests client MAY send an Array filled with Request objects.
// See spec https://www.jsonrpc.org/specification#batch page.
type BatchRequests []*Request

// BatchResponses the Server should respond with an Array containing the corresponding Response objects,
// after all of the batch Request objects have been processed.
type BatchResponses []*Response
