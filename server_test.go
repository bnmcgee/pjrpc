package pjrpc_test

import (
	"bytes"
	"context"
	"encoding/json"
	"log"
	"strings"
	"testing"

	"gitlab.com/pjrpc/pjrpc/v2"
)

func TestServer(t *testing.T) {
	t.Parallel()

	srv := pjrpc.NewServer()

	testCase := func(body, want string) {
		t.Helper()

		got := srv.Serve(context.Background(), []byte(body))
		if string(got) != want {
			t.Fatalf("wrong result\nwant: %s\ngot:  %s", want, got)
		}
	}

	srv.RegisterMethod("regular", func(ctx context.Context, params json.RawMessage) (interface{}, error) {
		switch string(params) {
		case `"want ok"`:
			return "ok", nil

		case `"want error"`:
			return nil, errTest

		case `"want json-rpc error"`:
			return nil, pjrpc.JRPCErrInternalError("json-rpc error")

		case `"want panic"`:
			panic("panic Booo!")
		}

		return "unknown params", nil
	})

	// Positives:
	testCase(
		`{"jsonrpc":"2.0","id":"id","method":"regular","params":"want ok"}`,
		`{"jsonrpc":"2.0","id":"id","result":"ok"}`,
	)
	testCase(
		`{"jsonrpc":"2.0","id":123,"method":"regular","params":"want ok"}`,
		`{"jsonrpc":"2.0","id":123,"result":"ok"}`,
	)
	testCase(
		`{"jsonrpc":"2.0","id":123,"method":"regular","params":"want error"}`,
		`{"jsonrpc":"2.0","id":123,"error":{"code":-32000,"message":"Server error","data":"method 'regular': test error"}}`,
	)
	testCase(
		`{"jsonrpc":"2.0","id":123,"method":"regular","params":"want json-rpc error"}`,
		`{"jsonrpc":"2.0","id":123,"error":{"code":-32603,"message":"Internal error","data":"json-rpc error"}}`,
	)

	// Notification:
	testCase(
		`{"jsonrpc":"2.0","method":"regular","params":"want ok"}`,
		``,
	)
	testCase(
		`{"jsonrpc":"2.0","method":"regular"}`,
		``,
	)

	// Batch:
	testCase(
		`[
			{"jsonrpc":"2.0","id":"id","method":"regular","params":"want ok"},
			{"jsonrpc":"2.0","method":"regular","params":"want ok"},
			{"jsonrpc":"2.0","id":123,"method":"regular","params":"want json-rpc error"},
			{"jsonrpc":"2.0","id":321,"method":"foobar","params":{}},
			{"foo":"bar"}
		]`,
		`[`+
			`{"jsonrpc":"2.0","id":"id","result":"ok"},`+
			`{"jsonrpc":"2.0","id":123,"error":{"code":-32603,"message":"Internal error","data":"json-rpc error"}},`+
			`{"jsonrpc":"2.0","id":321,"error":{"code":-32601,"message":"Method not found"}},`+
			`{"jsonrpc":"2.0","error":{"code":-32600,"message":"Invalid Request","data":"wrong value of the field 'jsonrpc'"}}`+
			`]`,
	)

	testCase(
		`[
			{"jsonrpc":"2.0","method":"regular","params":"notify"},
			{"jsonrpc":"2.0","method":"regular","params":"notify"}
		]`,
		``,
	)

	// Negatives:
	testCase(
		`{"jsonrpc":"2.0","id":123,"method":"regular","params":{"Bad json"}}`,
		`{"jsonrpc":"2.0","error":{"code":-32600,"message":"Invalid Request","data":"failed to parse request"}}`,
	)
	testCase(
		`{"jsonrpc":"1.0","id":"id","method":"regular","params":"want ok"}`,
		`{"jsonrpc":"2.0","id":"id","error":{"code":-32600,"message":"Invalid Request","data":"wrong value of the field 'jsonrpc'"}}`,
	)
	testCase(
		`{"jsonrpc":"2.0","id":"id","method":"regular","params":"want panic"}`,
		`{"jsonrpc":"2.0","id":"id","error":{"code":-32603,"message":"Internal error"}}`,
	)
	testCase(
		`{"jsonrpc":"2.0","method":"foobar","id":"1"}`,
		`{"jsonrpc":"2.0","id":"1","error":{"code":-32601,"message":"Method not found"}}`,
	)
	testCase(
		`{"jsonrpc":"2.0","method":"foobar"}`,
		``,
	)
	testCase(
		`[1]`,
		`[{"jsonrpc":"2.0","error":{"code":-32600,"message":"Invalid Request","data":"failed to parse request"}}]`,
	)
	testCase(
		`[1,2,3]`,
		`[{"jsonrpc":"2.0","error":{"code":-32600,"message":"Invalid Request","data":"failed to parse request"}}]`,
	)
	testCase(
		`[]`,
		`[{"jsonrpc":"2.0","error":{"code":-32600,"message":"Invalid Request"}}]`,
	)
	testCase(
		``,
		`{"jsonrpc":"2.0","error":{"code":-32600,"message":"Invalid Request","data":"message length is 0"}}`,
	)

	// Panic handler, no logger, no handler -> Default logger:
	logger := bytes.NewBuffer(nil)
	log.Default().SetOutput(logger)
	srv.OnPanic = nil

	testCase(
		`{"jsonrpc":"2.0","id":"id","method":"regular","params":"want panic"}`,
		`{"jsonrpc":"2.0","id":"id","error":{"code":-32603,"message":"Internal error"}}`,
	)
	if !strings.Contains(logger.String(), "panic Booo!") {
		t.Fatal(logger.String())
	}

	// Custom logger.
	logger.Reset()
	srv.SetLogger(logger)

	testCase(
		`{"jsonrpc":"2.0","id":"id","method":"regular","params":"want panic"}`,
		`{"jsonrpc":"2.0","id":"id","error":{"code":-32603,"message":"Internal error"}}`,
	)
	if !strings.Contains(logger.String(), "panic Booo!") {
		t.Fatal(logger.String())
	}

	// Middleware:
	var called bool
	mw := func(next pjrpc.Handler) pjrpc.Handler {
		return func(ctx context.Context, params json.RawMessage) (interface{}, error) {
			called = true
			return next(ctx, params)
		}
	}

	srv.With(mw)
	testCase(
		`{"jsonrpc":"2.0","id":"id","method":"regular","params":"want ok"}`,
		`{"jsonrpc":"2.0","id":"id","result":"ok"}`,
	)
	if !called {
		t.Fatal("did't call mw")
	}
}

// Server can serve with empty context or with prepared.
func TestServe_CheckContext(t *testing.T) {
	t.Parallel()

	srv := pjrpc.NewServer()
	ctx := context.Background()
	preparedData := &pjrpc.ContextData{}
	wantResult := `{"jsonrpc":"2.0","id":"id","result":"result"}`

	srv.RegisterMethod("ctx", func(ctx context.Context, params json.RawMessage) (interface{}, error) {
		t.Helper()

		data, ok := pjrpc.ContextGetData(ctx)
		if !ok {
			t.Fatal("no data in context")
		}

		if data.JRPCRequest.Method != "ctx" {
			t.Fatal("wrong method:", data.JRPCRequest.Method)
		}

		switch string(params) {
		case `"empty"`:
			if data == preparedData {
				return "has the same data", nil
			}
		case `"prepared"`:
			if data != preparedData {
				return "has wrong data", nil
			}

		default:
			return "wrong params:" + string(params), nil
		}

		return "result", nil
	})

	result := srv.Serve(ctx, []byte(`{"jsonrpc":"2.0","id":"id","method":"ctx","params":"empty"}`))
	if string(result) != wantResult {
		t.Fatal(string(result))
	}

	ctx = pjrpc.ContextSetData(ctx, preparedData)

	result = srv.Serve(ctx, []byte(`{"jsonrpc":"2.0","id":"id","method":"ctx","params":"prepared"}`))
	if string(result) != wantResult {
		t.Fatal(string(result))
	}
}
