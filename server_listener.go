package pjrpc

import (
	"context"
	"encoding/json"
	"errors"
	"io"
	"net"

	"gitlab.com/pjrpc/pjrpc/v2/pjson"
)

var _ Registrator = &ServerListener{}

// ServerListener JSON-RPC server based on net.Listener.
// You can use it as JSON-RPC server over TCP or other protocols.
// Look for "On..." method of the server to set your own callbacks.
type ServerListener struct {
	*Server

	Listener net.Listener

	// OnErrorAccept takes error after attempt to accept a new connection.
	// If you don't want to stop the listener you can return nil here.
	OnErrorAccept func(err error) error

	// OnNewConnection it called on each new accepted connection.
	// You can return false here if you want to refuse the connection.
	OnNewConnection func(conn net.Conn) (accept bool)

	// OnCloseConnection it called before each closure of the connection.
	// If you refused the connection in the OnNewConnection method,
	// you will not get that connection here.
	OnCloseConnection func(conn net.Conn)

	// OnErrorReceive it called when Handler gets reading error from the connection.
	// By default the connection will be closed.
	OnErrorReceive func(conn net.Conn, err error) (needClose bool)

	// OnErrorSend it called when Handler gets writing error from the connection.
	// By default the connection will be closed.
	OnErrorSend func(conn net.Conn, err error) (needClose bool)
}

// NewServerListener returns a new Server with default handler's callbacks.
func NewServerListener(listener net.Listener) *ServerListener {
	return &ServerListener{
		Server:   NewServer(),
		Listener: listener,

		OnErrorAccept:     func(err error) error { return err },
		OnNewConnection:   func(conn net.Conn) (accept bool) { return true },
		OnCloseConnection: func(conn net.Conn) {},
		OnErrorReceive:    func(conn net.Conn, err error) (needClose bool) { return true },
		OnErrorSend:       func(conn net.Conn, err error) (needClose bool) { return true },
	}
}

// Listen starts to listen the net.Listener for a new connections from the network.
func (s *ServerListener) Listen() error {
	for {
		conn, err := s.Listener.Accept()
		if err != nil {
			if errors.Is(err, net.ErrClosed) {
				return nil // Just stop without error.
			}

			if acceptErr := s.OnErrorAccept(err); acceptErr != nil {
				return acceptErr
			}

			continue
		}

		ctx := ContextSetConnection(context.Background(), conn)
		ctx = ContextSetData(ctx, &ContextData{})

		go s.Handler(ctx, conn)
	}
}

// Close closes the net.Listener of the server.
func (s *ServerListener) Close() error {
	return s.Listener.Close() //nolint:wrapcheck // no need to wrap single error.
}

// Handler takes enriched context and connection from the network.
// It calls handler callbacks and listens the connection for messages.
func (s *ServerListener) Handler(ctx context.Context, conn net.Conn) {
	defer conn.Close() //nolint:errcheck // just ignore closing error.

	if !s.OnNewConnection(conn) {
		return
	}

	defer s.OnCloseConnection(conn)

	var (
		dec = pjson.NewDecoder(conn)
		msg json.RawMessage
	)

	for {
		msg = msg[:0]

		err := dec.Decode(&msg)
		if err != nil {
			if errors.Is(err, io.EOF) || s.OnErrorReceive(conn, err) {
				return
			}

			// Decoder remembers the last error and OnErrorReceive doesn't help here.
			dec = pjson.NewDecoder(conn)
			continue
		}

		resp := s.Server.Serve(ctx, msg)

		if resp == nil { // It was notification.
			continue
		}

		_, err = conn.Write(resp)
		if err != nil && s.OnErrorSend(conn, err) {
			return
		}
	}
}
