module gitlab.com/pjrpc/pjrpc/ws

go 1.17

require (
	gitlab.com/pjrpc/pjrpc/v2 v2.0.0
	golang.org/x/net v0.5.0
)

replace gitlab.com/pjrpc/pjrpc/v2 => ../
