package ws_test

import (
	"context"
	"fmt"
	"net"
	"net/http"
	"time"

	"golang.org/x/net/websocket"

	"gitlab.com/pjrpc/pjrpc/v2"
	"gitlab.com/pjrpc/pjrpc/v2/client"
	"gitlab.com/pjrpc/pjrpc/ws"
)

// Example about websocket server and notifications from server.
func Example() {
	address := "localhost:8080"

	wsConfig, err := websocket.NewConfig(fmt.Sprintf("ws://%s/ws", address), "http://localhost")
	if err != nil {
		fmt.Println("websocket.NewConfig:", err)
		return
	}

	conf := ws.Config{
		WSConfig: wsConfig,

		Handshake: func(conf *websocket.Config, r *http.Request) error {
			return nil // You can validate http request here.
		},
	}

	wsServer := ws.NewServerWebsocket(conf)

	clients := make(chan *client.AsyncClient)

	wsServer.OnNewConnection = func(conn net.Conn) bool {
		fmt.Println("Got a new connection")

		// You can unwrap the connection if you need it.
		_, ok := conn.(*websocket.Conn)
		if !ok {
			fmt.Println("it is not websocket connection")
		}

		cl := client.NewAsyncClient(conn)

		// Don't call cl.Listen() when your server already listens this connection.

		clients <- cl

		// You can return false if you don't want to accept this connection.
		return true
	}

	wsServer.OnCloseConnection = func(conn net.Conn) {
		// Just remove client from your notifiction logic.
		// You don't need close notification client here (cl.Close).
	}

	mux := http.NewServeMux()
	mux.Handle("/ws", wsServer)

	go func() {
		err = http.ListenAndServe(address, mux)
		if err != nil {
			fmt.Println("http.ListenAndServe:", err)
		}
	}()

	clientGotMessage := make(chan string)

	go func() {
		conn, errDial := websocket.Dial(fmt.Sprintf("ws://%s/ws", address), "", "http://localhost")
		if errDial != nil {
			fmt.Println("websocket.Dial:", errDial)
			return
		}

		var msg string

		err = websocket.Message.Receive(conn, &msg)
		if err != nil {
			fmt.Println("websocket.Receive:", err)
			return
		}

		clientGotMessage <- msg
	}()

	// Waiting for a new client.
	cl := <-clients

	err = cl.Invoke(context.Background(), "", "notify", "string as parameter", nil)
	if err != nil {
		fmt.Println("client.Invoke:", err)
		return
	}

	// Waiting for a message that got the client.
	fmt.Println(<-clientGotMessage)

	// Output:
	// Got a new connection
	// {"jsonrpc":"2.0","method":"notify","params":"string as parameter"}
}

//nolint:testableexamples // this is example is not testable because it works with real server.
func ExampleNewClient() {
	// Create x/net/websocket.Config.
	config, err := websocket.NewConfig("ws://websocket-echo.com", "http://websocket-echo.com")
	if err != nil {
		fmt.Println("websocket.NewConfig:", err)
		return
	}

	// Create client and dial to the server.
	wsClient, err := ws.NewClient(config)
	if err != nil {
		fmt.Println("client.New:", err)
		return
	}

	// Optional: set unexpected handlers.
	wsClient.OnParseMessageError = func(err error) error {
		fmt.Println("failed to parse message:", err)
		return nil // Return nil if you don't want to stop listener.
	}

	wsClient.OnUnknownResponse = func(resp *pjrpc.Response) error {
		fmt.Println("unknown response:", resp.GetID())
		return nil // Return nil if you don't want to stop listener.
	}

	// Run response listener.
	go func() {
		err = wsClient.Listen()
		if err != nil {
			fmt.Println("wsClient.Listen:", err)
		}

		wsClient.Close() // Don't worry about error here.
	}()

	// Invoke the method.
	var res interface{}

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*3)
	defer cancel()

	err = wsClient.Invoke(ctx, "123", "test", "params", &res)
	if err != nil {
		fmt.Println("wsClient.Invoke:", err)
		return
	}

	fmt.Println(res) // You will got <nil> as result because it's echo server.
}
