# Websocket

JSON-RPC client/server over Websocket protocol.

This package contains Webscoket server based on `golang.org/x/net/websocket` package.
And contains Websocket full client that you can use to send notification from server.

See Server+Notification example in [_examples/websocket](https://gitlab.com/pjrpc/pjrpc/-/tree/master/_examples/websocket) directory.
