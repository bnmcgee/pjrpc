// Package ws contains JSON-RPC server over Websocket protocol.
package ws

import (
	"fmt"
	"net/http"

	"golang.org/x/net/websocket"

	"gitlab.com/pjrpc/pjrpc/v2"
	"gitlab.com/pjrpc/pjrpc/v2/client"
)

var (
	_ http.Handler      = &ServerWebsocket{}
	_ pjrpc.Registrator = &ServerWebsocket{}
)

// Config to create websocket server.
// WSConfig is required field, feel free to set nil on others.
type Config struct {
	// Required config of the Websocket server.
	WSConfig *websocket.Config

	// Handshake handshake of the HTTP request.
	// You can refuse connection here.
	Handshake func(conf *websocket.Config, r *http.Request) error
}

// ServerWebsocket JSON-RPC server over WebSocket protocol.
type ServerWebsocket struct {
	*pjrpc.ServerListener

	Websocket *websocket.Server
}

// NewServerWebsocket returns new Websocket Server based on your config.
func NewServerWebsocket(config Config) *ServerWebsocket {
	s := &ServerWebsocket{
		ServerListener: pjrpc.NewServerListener(nil),
		Websocket: &websocket.Server{
			Config:    *config.WSConfig,
			Handshake: config.Handshake,
		},
	}

	s.Websocket.Handler = func(conn *websocket.Conn) {
		ctx := pjrpc.ContextSetConnection(conn.Request().Context(), conn)
		ctx = pjrpc.ContextSetData(ctx, &pjrpc.ContextData{HTTTRequest: conn.Request()})

		s.ServerListener.Handler(ctx, conn)
	}

	return s
}

// ServeHTTP implements the http.Handler interface for a WebSocket server.
func (s *ServerWebsocket) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	s.Websocket.ServeHTTP(w, r)
}

// NewClient returns new async client based on websocket connection.
func NewClient(config *websocket.Config) (*client.AsyncClient, error) {
	conn, err := websocket.DialConfig(config)
	if err != nil {
		return nil, fmt.Errorf("websocket.DialConfig: %w", err)
	}

	return client.NewAsyncClient(conn), nil
}
