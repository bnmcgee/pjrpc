package ws_test

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"net/http"
	"net/http/httptest"
	"testing"
	"time"

	"golang.org/x/net/websocket"

	"gitlab.com/pjrpc/pjrpc/v2"
	"gitlab.com/pjrpc/pjrpc/ws"
)

type params struct {
	Name string
}

//nolint:gocognit // little complex test because a lot of checks in the rpc handler.
func TestServerWebsocket(t *testing.T) {
	t.Parallel()
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*3)
	defer cancel()

	mux := http.NewServeMux()
	httpServer := httptest.NewServer(mux)

	wsConfig, err := websocket.NewConfig(fmt.Sprintf("ws://%s/ws", httpServer.Listener.Addr()), "http://localhost")
	if err != nil {
		t.Fatal(err)
	}

	gotConnect := make(chan struct{})

	conf := ws.Config{
		WSConfig: wsConfig,
		Handshake: func(conf *websocket.Config, r *http.Request) error {
			close(gotConnect)
			return nil
		},
	}

	wsServer := ws.NewServerWebsocket(conf)

	var wantParams []byte
	count := 0

	wsServer.RegisterMethod("test", func(ctx context.Context, gotParams json.RawMessage) (interface{}, error) {
		if !bytes.Equal(wantParams, gotParams) {
			t.Fatalf("got wrong params:\nwant: %s\ngot:  %s", wantParams, gotParams)
		}

		data, ok := pjrpc.ContextGetData(ctx)
		if !ok {
			t.Fatal("no JSON-RPC context")
		}

		conn, ok := pjrpc.ContextGetConnection(ctx)
		if !ok {
			t.Fatal("no connection in context")
		}

		wsConn, ok := conn.(*websocket.Conn)
		if !ok {
			t.Fatalf("connection is not websocket: %T", conn)
		}

		if wsConn.Request() != data.HTTTRequest {
			t.Fatal("wrong request in context")
		}

		if data.JRPCRequest.Method != "test" {
			t.Fatal("wrong method:", data.JRPCRequest.Method)
		}

		count++

		return params{Name: fmt.Sprintf("response %d", count)}, nil
	})

	mux.Handle("/ws", wsServer)

	wsClient, err := ws.NewClient(wsConfig)
	if err != nil {
		t.Fatal(err)
	}

	go func() {
		wsClient.Listen()
		wsClient.Close()
	}()

	<-gotConnect

	var res params

	wantParams = []byte(`{"Name":"first request"}`)
	err = wsClient.Invoke(ctx, "1", "test", params{Name: "first request"}, &res)
	if err != nil {
		t.Fatal(err)
	}

	if res.Name != "response 1" {
		t.Fatal("got wrong response:", res.Name)
	}

	wantParams = []byte(`{"Name":"2nd request"}`)
	err = wsClient.Invoke(ctx, "2", "test", params{Name: "2nd request"}, &res)
	if err != nil {
		t.Fatal(err)
	}

	if res.Name != "response 2" {
		t.Fatal("got wrong response:", res.Name)
	}
}

func TestNewClient(t *testing.T) {
	t.Parallel()

	wsConfig, err := websocket.NewConfig("ws://localhost/ws", "http://localhost")
	if err != nil {
		t.Fatal(err)
	}

	_, err = ws.NewClient(wsConfig)
	if err == nil {
		t.Fatal("must be error")
	}
}
