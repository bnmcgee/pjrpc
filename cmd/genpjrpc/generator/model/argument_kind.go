package model

import (
	"go/types"
)

// ArgumentFormat additional format of the argument.
type ArgumentFormat string

const (
	// ArgumentFormatTime means time.Time type.
	ArgumentFormatTime ArgumentFormat = "date-time"
	// ArgumentFormatUUID means UUID string type.
	ArgumentFormatUUID ArgumentFormat = "uuid"
)

// ArgumentKind describes the kind of basic type.
type ArgumentKind int

// List of the kinds.
const (
	ArgumentKindInvalid ArgumentKind = iota
	ArgumentKindBool
	ArgumentKindInt
	ArgumentKindInt8
	ArgumentKindInt16
	ArgumentKindInt32
	ArgumentKindInt64
	ArgumentKindUint
	ArgumentKindUint8
	ArgumentKindUint16
	ArgumentKindUint32
	ArgumentKindUint64
	ArgumentKindFloat32
	ArgumentKindFloat64
	ArgumentKindString
)

var stringToArgumentKind = map[string]ArgumentKind{
	"invalid": ArgumentKindInvalid,
	"bool":    ArgumentKindBool,
	"int":     ArgumentKindInt,
	"int8":    ArgumentKindInt8,
	"int16":   ArgumentKindInt16,
	"int32":   ArgumentKindInt32,
	"int64":   ArgumentKindInt64,
	"uint":    ArgumentKindUint,
	"uint8":   ArgumentKindUint8,
	"uint16":  ArgumentKindUint16,
	"uint32":  ArgumentKindUint32,
	"uint64":  ArgumentKindUint64,
	"float32": ArgumentKindFloat32,
	"float64": ArgumentKindFloat64,
	"string":  ArgumentKindString,
}

var argumentKindToString = map[ArgumentKind]string{
	ArgumentKindInvalid: "invalid",
	ArgumentKindBool:    "bool",
	ArgumentKindInt:     "int",
	ArgumentKindInt8:    "int8",
	ArgumentKindInt16:   "int16",
	ArgumentKindInt32:   "int32",
	ArgumentKindInt64:   "int64",
	ArgumentKindUint:    "uint",
	ArgumentKindUint8:   "uint8",
	ArgumentKindUint16:  "uint16",
	ArgumentKindUint32:  "uint32",
	ArgumentKindUint64:  "uint64",
	ArgumentKindFloat32: "float32",
	ArgumentKindFloat64: "float64",
	ArgumentKindString:  "string",
}

// TypesKindToArgumentKind converts go.types basic kind to ArgumentKind.
var TypesKindToArgumentKind = map[types.BasicKind]ArgumentKind{
	types.Bool:    ArgumentKindBool,
	types.Int:     ArgumentKindInt,
	types.Int8:    ArgumentKindInt8,
	types.Int16:   ArgumentKindInt16,
	types.Int32:   ArgumentKindInt32,
	types.Int64:   ArgumentKindInt64,
	types.Uint:    ArgumentKindUint,
	types.Uint8:   ArgumentKindUint8,
	types.Uint16:  ArgumentKindUint16,
	types.Uint32:  ArgumentKindUint32,
	types.Uint64:  ArgumentKindUint64,
	types.Float32: ArgumentKindFloat32,
	types.Float64: ArgumentKindFloat64,
	types.String:  ArgumentKindString,
}

// String converts int value to string name.
func (ak ArgumentKind) String() string {
	res, ok := argumentKindToString[ak]
	if !ok {
		return "unknown"
	}

	return res
}

// GetArgumentKind returns ArgumentKind using on text name.
func GetArgumentKind(text string) ArgumentKind {
	return stringToArgumentKind[text]
}
