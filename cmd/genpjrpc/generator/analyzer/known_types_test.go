package analyzer_test

import (
	"encoding/json"
	"os"
	"path/filepath"
	"testing"

	"github.com/stretchr/testify/require"

	"gitlab.com/pjrpc/pjrpc/cmd/genpjrpc/generator/analyzer"
	"gitlab.com/pjrpc/pjrpc/cmd/genpjrpc/generator/model"
)

func TestParseKnownTypes(t *testing.T) {
	t.Parallel()
	so := require.New(t)

	res, err := analyzer.ParseKnownTypes("")
	so.NoError(err)
	so.Len(res, 0)

	testDir := filepath.Join("..", "..", "testdata", "service")

	res, err = analyzer.ParseKnownTypes(filepath.Join(testDir, "known_types.json"))
	so.NoError(err)
	so.Len(res, 1)

	so.Equal(
		model.Argument{BasicKind: model.ArgumentKindInt64, BasicFormat: model.ArgumentFormat("integer")},
		res["github.com/shopspring/decimal.Decimal"],
	)

	_, err = analyzer.ParseKnownTypes(filepath.Join(testDir, "known_types_invalid.json"))
	so.ErrorIs(err, analyzer.ErrInvalidData)

	_, err = analyzer.ParseKnownTypes("file.json")
	so.ErrorIs(err, os.ErrNotExist)

	_, err = analyzer.ParseKnownTypes("known_types_test.go")
	var wantErr *json.SyntaxError
	so.ErrorAs(err, &wantErr)
}
