package analyzer

import (
	"fmt"
	"go/ast"
	"reflect"
	"strconv"
	"strings"

	"gitlab.com/pjrpc/pjrpc/cmd/genpjrpc/generator/model"
)

// JoinComments joins some groups of comments.
func JoinComments(comments ...*ast.CommentGroup) string {
	texts := make([]string, len(comments))

	for i, comment := range comments {
		texts[i] = comment.Text()
	}

	return strings.Join(texts, "")
}

// FieldTag represents parsed field's tag.
type FieldTag struct {
	Name         string             // JSON tag's name or origin field's name.
	SwaggerKind  model.ArgumentKind // Type of the swagger.
	IsSkipped    bool               // The fields should be skipped in swagger spec.
	IsOptional   bool               // The field has the omitempty tag.
	IsDeprecated bool               // The field has deprecated mark in the tag.
}

const (
	tagNameJSON               = "json"
	tagNameGenpjrpc           = "genpjrpc"
	tagNameGenpjrpcType       = "type"
	tagNameGenpjrpcDeprecated = "deprecated"
)

func parseJSONTag(dst *FieldTag, tag reflect.StructTag) {
	value, ok := tag.Lookup(tagNameJSON)
	if !ok {
		return
	}

	values := strings.Split(value, ",")

	fieldName := values[0]

	if fieldName == "-" {
		dst.IsSkipped = true
		return
	}

	if fieldName != "" {
		dst.Name = fieldName
	}

	for _, v := range values[1:] {
		if v == "omitempty" {
			dst.IsOptional = true
			break
		}
	}
}

func parseStructTagParam(dst *FieldTag, param string) error {
	parsedParams := strings.Split(param, ":")
	if len(parsedParams) != 2 {
		return fmt.Errorf("%w: wrong tag param", ErrInvalidData)
	}

	key, value := parsedParams[0], parsedParams[1]

	switch key {
	case tagNameGenpjrpcType:
		dst.SwaggerKind = model.GetArgumentKind(value)
		if dst.SwaggerKind == model.ArgumentKindInvalid {
			return fmt.Errorf("%w: unknown basic type in '%s': %q", ErrInvalidData, tagNameGenpjrpcType, value)
		}

	case tagNameGenpjrpcDeprecated:
		boolValue, err := strconv.ParseBool(value)
		if err != nil {
			return fmt.Errorf("faled to parse '%s' value: %w", tagNameGenpjrpcDeprecated, err)
		}

		dst.IsDeprecated = boolValue

	default:
		return fmt.Errorf("%w: unknown param's key %q", ErrInvalidData, key)
	}

	return nil
}

func parseStructTag(dst *FieldTag, tag reflect.StructTag) error {
	value, ok := tag.Lookup(tagNameGenpjrpc)
	if !ok || value == "" {
		return nil
	}

	for _, param := range strings.Split(value, ",") {
		if err := parseStructTagParam(dst, param); err != nil {
			return fmt.Errorf("failed to parse '%s' tag param %q: %w", tagNameGenpjrpc, param, err)
		}
	}

	return nil
}

// ParseStructTag returns parsed field's tag with params.
func ParseStructTag(fieldTag *ast.BasicLit, fieldName string) (*FieldTag, error) {
	res := &FieldTag{
		Name: fieldName, // By default.
	}

	if fieldTag == nil {
		return res, nil
	}

	tag := reflect.StructTag(strings.Trim(fieldTag.Value, "`"))

	parseJSONTag(res, tag)

	if err := parseStructTag(res, tag); err != nil {
		return nil, fmt.Errorf("parseSwaggerTypeTag: %w", err)
	}

	return res, nil
}

const (
	magicCommentPrefix      = "//genpjrpc:params"
	magicCommentMethodName  = "method_name"
	magicCommentSwaggerTags = "swagger_tags"
	magicCommentDeprecated  = "deprecated"
)

// MagicCommentParams parsed magic comments.
type MagicCommentParams struct {
	MethodName   string
	SwaggerTags  []string
	IsDeprecated bool
}

func parseMagicCommentParam(dst *MagicCommentParams, param string) error {
	parsedParam := strings.Split(param, "=")
	if len(parsedParam) != 2 {
		return fmt.Errorf("%w: should be key=value", ErrInvalidData)
	}

	key, value := parsedParam[0], parsedParam[1]

	switch key {
	case magicCommentMethodName:
		dst.MethodName = value

	case magicCommentSwaggerTags:
		dst.SwaggerTags = strings.Split(value, ",")

	case magicCommentDeprecated:
		boolValue, err := strconv.ParseBool(value)
		if err != nil {
			return fmt.Errorf("faled to parse 'deprecated' value: %w", err)
		}

		dst.IsDeprecated = boolValue

	default:
		return fmt.Errorf("%w: unknown param's key %q", ErrInvalidData, key)
	}

	return nil
}

func parseMagicComment(dst *MagicCommentParams, comment string) error {
	for _, param := range strings.Split(comment, " ") {
		if param == "" {
			continue
		}

		if err := parseMagicCommentParam(dst, param); err != nil {
			return fmt.Errorf("param %q: %w", param, err)
		}
	}

	return nil
}

// ParseMagicComments returns parameter from magic comment of the genpjrpc comment.
func ParseMagicComments(doc *ast.CommentGroup) (*MagicCommentParams, error) {
	res := new(MagicCommentParams)

	if doc == nil {
		return res, nil
	}

	for _, line := range doc.List {
		if !strings.HasPrefix(line.Text, magicCommentPrefix) {
			continue
		}

		comment := strings.TrimPrefix(line.Text, magicCommentPrefix)

		if err := parseMagicComment(res, comment); err != nil {
			return nil, fmt.Errorf("faield to parse magic comment (%q): %w", comment, err)
		}
	}

	return res, nil
}
