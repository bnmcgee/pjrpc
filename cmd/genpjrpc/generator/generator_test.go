package generator_test

import (
	"bytes"
	"context"
	"os"
	"path/filepath"
	"testing"
	"time"

	"github.com/rs/zerolog"
	"github.com/stretchr/testify/require"
	"gitlab.com/so_literate/gentools/logger"

	"gitlab.com/pjrpc/pjrpc/cmd/genpjrpc/config"
	"gitlab.com/pjrpc/pjrpc/cmd/genpjrpc/generator"
)

func getLogger() (*bytes.Buffer, *zerolog.Logger) {
	buf := bytes.NewBuffer([]byte("\n"))

	conf := &logger.Config{
		Debug:   true,
		Pretty:  true,
		NoColor: true,
		Writer:  buf,
	}

	log := logger.New(conf)

	return buf, log
}

func TestRun(t *testing.T) {
	t.Parallel()
	so := require.New(t)

	testdataDir := filepath.Join("..", "testdata")
	dstPath := filepath.Join(t.TempDir(), "gitlab.com", "repo", "project", "service")

	conf := &config.Config{
		Log: config.Log{}, // It will be replaced.
		Search: config.Search{
			Name: "Service",
			Path: filepath.Join(testdataDir, "service"),
		},
		Print: config.Print{
			Place: config.Place{
				Stdout:          false,
				PathServer:      dstPath,
				PathClient:      dstPath,
				PathSwaggerFile: filepath.Join(dstPath, "swagger.json"),
			},
			Content: config.Content{
				HideVersion:        false,
				TagsServer:         "go:build testfiles",
				TagsClients:        "go:build testfiles",
				SwaggerInfoVersion: "v0.0.0-go-test",
			},
		},
		Timeout: time.Minute,
		Version: false,
	}

	g, err := generator.New(conf, nil)
	so.NoError(err)

	buf, log := getLogger()
	g.Log = log

	err = g.Run(context.Background())
	so.NoError(err)

	t.Log("log out:", buf.String())

	fileAssert := func(want, got string) {
		t.Helper()

		wantFile, err := os.ReadFile(want)
		so.NoError(err)

		gotFile, err := os.ReadFile(got)
		so.NoError(err)

		so.Equal(string(wantFile), string(gotFile))
	}

	expectedDir := filepath.Join(testdataDir, "expected")

	fileAssert(
		filepath.Join(expectedDir, "pjrpc_client_service.go"),
		filepath.Join(dstPath, "pjrpc_client_service.go"),
	)

	fileAssert(
		filepath.Join(expectedDir, "pjrpc_server_service.go"),
		filepath.Join(dstPath, "pjrpc_server_service.go"),
	)

	fileAssert(
		filepath.Join(expectedDir, "swagger.json"),
		filepath.Join(dstPath, "swagger.json"),
	)
}
