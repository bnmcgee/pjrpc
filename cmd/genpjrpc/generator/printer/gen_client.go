package printer

import (
	"bytes"
	"fmt"

	"gitlab.com/pjrpc/pjrpc/cmd/genpjrpc/generator/model"
	"gitlab.com/pjrpc/pjrpc/cmd/genpjrpc/generator/printer/tmpl"
)

func (p *Printer) generateClientMethodConstants(dst *bytes.Buffer, serivce *model.Service) {
	dst.WriteString("// List of the client JSON-RPC methods.\n")
	dst.WriteString("const (\n")

	constNameData := &tmpl.MethodConstantNameData{ConstSuffix: "_Client"}
	constValueData := &tmpl.MethodConstantValueData{}

	if p.conf.Content.WithServiceName {
		constNameData.GoServiceName = serivce.GoName
		constValueData.JRPCServiceName = serivce.Name
	}

	p.clientMethodConstants = make(map[*model.Method]string, len(serivce.Methods))

	for _, method := range serivce.Methods {
		constNameData.GoMethodName = method.GoName
		constValueData.JRPCMethodName = method.Name

		constName := tmpl.ExecMethodConstantName(constNameData)
		constValue := tmpl.ExecMethodConstantValue(constValueData)

		p.clientMethodConstants[method] = constName

		fmt.Fprintf(dst, "\t%s = %q\n", constName, constValue)
	}

	dst.WriteString(")\n\n")
}

func (p *Printer) generateClientInterface(dst *bytes.Buffer, serivce *model.Service) string {
	interfaceName := fmt.Sprintf("%sClient", serivce.GoName)

	fmt.Fprintf(dst, "// %s is an API client for %s service.\ntype %s interface {\n", interfaceName, serivce.GoName, interfaceName)

	modType := p.importer.GetQualifiedObject(pkgPJRPCClient, "Mod")

	for i, method := range serivce.Methods {
		printOptionalNewLineInInterfaceMethod(dst, method, i)
		p.generateInterfaceMethod(dst, method, modType, 1, true)
	}

	dst.WriteString("}\n\n")

	return interfaceName
}

func (p *Printer) generateClientImplementor(dst *bytes.Buffer, interfaceName string, serivce *model.Service) {
	dataImpl := &tmpl.ClientImplementorData{
		Implementor:   fmt.Sprintf("impl%sClient", serivce.GoName),
		InvokerType:   p.importer.GetQualifiedObject(pkgPJRPCClient, "Invoker"),
		InterfaceName: interfaceName,
		ServiceName:   serivce.GoName,
	}

	tmpl.ExecClientImplementorTmpl(dst, dataImpl)

	methodData := &tmpl.ClientImplementorMethodData{
		Implementor:  dataImpl.Implementor,
		MethodFunc:   "",
		UUIDFunc:     p.importer.GetQualifiedObject(pkgUUID, "NewUUID"),
		FmtErrorf:    p.importer.GetQualifiedObject(pkgFmt, "Errorf"),
		OutType:      "",
		ConstantName: "",
	}

	for _, method := range serivce.Methods {
		var (
			out          *model.Argument
			outIsPointer bool
			outType      string
			errName      string
		)

		in, isIsPonter := unpackArgumentType(method.Parameter)

		if !method.IsNotification {
			out, outIsPointer = unpackArgumentType(method.Result)
			outType = p.importer.GetQualifiedObject(out.NamedImport, out.NamedName)
			errName = "err"
		}

		ifaceMethodData := &tmpl.InterfaceMethodData{
			MethodName:   method.GoName,
			ContextType:  p.importer.GetQualifiedObject(pkgContext, "Context"),
			InIsPointer:  isIsPonter,
			InType:       p.importer.GetQualifiedObject(in.NamedImport, in.NamedName),
			ModType:      p.importer.GetQualifiedObject(pkgPJRPCClient, "Mod"),
			HasResult:    true,
			ResultName:   "result",
			OutIsPointer: outIsPointer,
			OutType:      outType,
			ErrName:      errName,
		}

		methodData.MethodFunc = tmpl.ExecInterfaceMethod(ifaceMethodData)
		methodData.ResultVar = "result"
		methodData.OutType = ifaceMethodData.OutType
		methodData.OutIsPointer = ifaceMethodData.OutIsPointer
		methodData.ConstantName = p.clientMethodConstants[method]
		methodData.IsNotify = method.IsNotification

		if method.Comments != "" {
			printComments(dst, method.Comments, 0)
		}

		if method.IsDeprecated {
			printDeprecatedComments(dst, 0)
		}

		tmpl.ExecClientImplementorMethodTmpl(dst, methodData)
	}
}

//nolint:dupl // it looks like generateServer but it is ok.
func (p *Printer) generateClient(serivce *model.Service) error {
	if p.conf.Place.PathClient == "" || p.conf.Place.PathClient == emptyString {
		return nil
	}

	p.clientFile = new(bytes.Buffer)

	packageName, err := p.setCurrentImportPath(serivce, p.conf.Place.PathClient)
	if err != nil {
		return fmt.Errorf("setCurrentImportPath: %w", err)
	}

	p.printHeaderGoFile(p.clientFile, packageName, p.conf.Content.TagsClients)

	fileBody := new(bytes.Buffer)

	// Constants.
	p.generateClientMethodConstants(fileBody, serivce)

	// Client interface.
	interfaceName := p.generateClientInterface(fileBody, serivce)

	// Client implementor.
	p.generateClientImplementor(fileBody, interfaceName, serivce)

	// Write import block and whole file's content into file.
	p.clientFile.WriteString(p.importer.GenerateFileImport() + "\n")

	fileBody.WriteTo(p.clientFile) //nolint:errcheck,gosec // it is just two bytes.Buffer objects.

	if err = formatGoCode(p.clientFile, getLocalPrefix(serivce.Import)); err != nil {
		return fmt.Errorf("formatGoCode: %w\n\n%s", err, p.clientFile.String())
	}

	return nil
}
