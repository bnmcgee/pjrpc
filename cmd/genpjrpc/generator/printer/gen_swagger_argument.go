package printer

import (
	"fmt"
	"strconv"
	"strings"

	"gitlab.com/so_literate/openapi"

	"gitlab.com/pjrpc/pjrpc/cmd/genpjrpc/generator/model"
)

func (p *Printer) generateSwaggerArgumentStructFieldProp(field *model.Argument) openapi.SchemaObjectItem {
	propSchema := p.generateSwaggerArgument(field)

	if propSchema.Reference.Name != "" {
		// I have to use OneOf wrap because I want to keep field's comments and deprecated mark.
		// Swagger can't parse $ref porp with other fields of the Schema.
		propSchema = &openapi.Schema{
			OneOf: []*openapi.Schema{propSchema.GetReferenceObject()},
		}
	}

	propSchema.Description = strings.TrimRight(field.StructFieldComment, "\n")
	propSchema.Deprecated = field.StructFieldIsDeprecated

	return openapi.SchemaObjectItem{
		Name:   field.StructFieldName,
		Schema: propSchema,
	}
}

func (p *Printer) generateSwaggerArgumentStructFields(fields []*model.Argument) (props openapi.SchemaObject, reqs []string) {
	props = make(openapi.SchemaObject, len(fields))
	reqs = make([]string, 0, len(fields))

	for i, field := range fields {
		if !field.StructFieldIsOptional {
			reqs = append(reqs, field.StructFieldName)
		}

		props[i] = p.generateSwaggerArgumentStructFieldProp(field)
	}

	return props, reqs
}

func (p *Printer) generateSwaggerArgumentEnum(arg *model.Argument) (basicType openapi.SchemaType, enums []interface{}, text string) {
	basicSchema := p.generateSwaggerArgumentBasic(&model.Argument{BasicKind: arg.EnumBasicKind})
	basicType = basicSchema.Type

	enums = make([]interface{}, len(arg.EnumElements))
	texts := make([]string, len(arg.EnumElements))

	for i, enum := range arg.EnumElements {
		//nolint:exhaustive // Default case will work fine.
		switch basicType {
		case openapi.SchemaTypeInteger:
			enums[i], _ = strconv.ParseInt(enum.EnumElementValue, 10, 64) //nolint:errcheck // this must to be integer.

		case openapi.SchemaTypeNumber:
			enums[i], _ = strconv.ParseFloat(enum.EnumElementValue, 64) //nolint:errcheck // this must to be float.

		default:
			enums[i] = enum.EnumElementValue
		}

		texts[i] = fmt.Sprintf("* `%s` - %s", enum.EnumElementValue, strings.TrimRight(enum.EnumElementComment, "\n"))
	}

	text = strings.Join(texts, "\n")

	return basicType, enums, text
}

func (p *Printer) generateSwaggerArgumentNamed(arg *model.Argument) *openapi.Schema {
	name := p.importer.GetQualifiedObject(arg.NamedImport, arg.NamedName)

	res, ok := p.swaggerNamedSchemas[name]
	if ok {
		return res
	}

	res = new(openapi.Schema)

	p.swaggerNamedSchemas[name] = res

	res.Reference = openapi.Reference{Name: name}

	res.Description = strings.TrimRight(arg.NamedComment, "\n")

	object := arg.NamedObject

	switch {
	case object.IsStruct():
		res.Type = openapi.SchemaTypeObject
		res.Properties, res.Required = p.generateSwaggerArgumentStructFields(object.StructFields)

	case object.IsEnum():
		var enumComments string
		res.Type, res.Enum, enumComments = p.generateSwaggerArgumentEnum(object)

		if res.Description != "" {
			res.Description += "\n"
		}

		res.Description += enumComments

	default:
		objectSchema := p.generateSwaggerArgument(object)
		res.Type = objectSchema.Type
		res.Format = objectSchema.Format
		res.Items = objectSchema.Items
		res.Properties = objectSchema.Properties
		res.AdditionalProperties = objectSchema.AdditionalProperties
	}

	return res
}

func (p *Printer) generateSwaggerArgumentBasic(arg *model.Argument) *openapi.Schema {
	res := new(openapi.Schema)

	var defaultFormat openapi.SchemaFormat

	switch arg.BasicKind {
	case model.ArgumentKindBool:
		res.Type = openapi.SchemaTypeBoolean

	case model.ArgumentKindString:
		res.Type = openapi.SchemaTypeString

	case model.ArgumentKindFloat32:
		res.Type = openapi.SchemaTypeNumber
		defaultFormat = openapi.SchemaFormatFloat

	case model.ArgumentKindFloat64:
		res.Type = openapi.SchemaTypeNumber
		defaultFormat = openapi.SchemaFormatDouble

	case model.ArgumentKindInt, model.ArgumentKindInt8, model.ArgumentKindInt16, model.ArgumentKindInt32, model.ArgumentKindInt64,
		model.ArgumentKindUint, model.ArgumentKindUint8, model.ArgumentKindUint16, model.ArgumentKindUint32, model.ArgumentKindUint64:
		res.Type = openapi.SchemaTypeInteger
		defaultFormat = openapi.SchemaFormat(arg.BasicKind.String())

	case model.ArgumentKindInvalid:
		res.Type = openapi.SchemaType(arg.BasicKind.String())
	}

	if arg.BasicFormat != "" {
		defaultFormat = openapi.SchemaFormat(arg.BasicFormat)
	}

	res.Format = defaultFormat

	return res
}

func (p *Printer) generateSwaggerArgumentSlice(arg *model.Argument) *openapi.Schema {
	itemType := p.generateSwaggerArgument(arg.SliceElement)

	if itemType.Reference.Name != "" {
		itemType = itemType.GetReferenceObject()
	}

	return &openapi.Schema{
		Type:  openapi.SchemaTypeArray,
		Items: itemType,
	}
}

func (p *Printer) generateSwaggerArgumentAny() *openapi.Schema {
	return &openapi.Schema{
		Description: "Can be anything",
		Nullable:    true,
		AnyOf: []*openapi.Schema{
			{Type: openapi.SchemaTypeInteger},
			{Type: openapi.SchemaTypeNumber},
			{Type: openapi.SchemaTypeString},
			{Type: openapi.SchemaTypeBoolean},
			{Type: openapi.SchemaTypeArray, Items: &openapi.Schema{}},
			{Type: openapi.SchemaTypeObject},
		},
	}
}

func (p *Printer) generateSwaggerArgumentMap(arg *model.Argument) *openapi.Schema {
	element := p.generateSwaggerArgument(arg.MapElem)
	if element.Reference.Name != "" {
		element = element.GetReferenceObject()
	}

	object := &openapi.Schema{
		Type: openapi.SchemaTypeObject,
	}

	// If key is a Enum then object will be created.
	if arg.MapKey.IsNamed() && arg.MapKey.NamedObject.IsEnum() {
		enum := arg.MapKey.NamedObject

		object.Properties = make(openapi.SchemaObject, len(enum.EnumElements))
		for i, value := range enum.EnumElements {
			object.Properties[i] = openapi.SchemaObjectItem{
				Name:   value.EnumElementValue,
				Schema: element,
			}
		}
	} else {
		// Otherwise iw will be just object with additionalProperties.
		object.AdditionalProperties = element
	}

	return object
}

func (p *Printer) generateSwaggerArgument(arg *model.Argument) *openapi.Schema {
	if arg.IsPointer() {
		return p.generateSwaggerArgument(arg.PointerElement)
	}

	if arg.IsNamed() {
		return p.generateSwaggerArgumentNamed(arg)
	}

	if arg.IsBasic() {
		return p.generateSwaggerArgumentBasic(arg)
	}

	if arg.IsSlice() {
		return p.generateSwaggerArgumentSlice(arg)
	}

	if arg.IsAny() {
		return p.generateSwaggerArgumentAny()
	}

	if arg.IsMap() {
		return p.generateSwaggerArgumentMap(arg)
	}

	wrongType := &openapi.Schema{
		Title:  "unsupported argument",
		Type:   openapi.SchemaType("invalid"),
		Format: openapi.SchemaFormat(fmt.Sprintf("%#v", arg)),
	}

	return wrongType
}

func (p *Printer) generateSwaggerArgumentErrorType(data *model.Argument) *openapi.Schema {
	res := &openapi.Schema{
		Reference:   openapi.Reference{Name: "_rpcError"},
		Title:       "rpcError",
		Description: `REQUIRED on error. This member MUST NOT exist if there was no error triggered during invocation.`,
		Type:        openapi.SchemaTypeObject,
		Properties: openapi.SchemaObject{
			{
				Name: "code",
				Schema: &openapi.Schema{
					Description: `A Number that indicates the error type that occurred.`,
					Type:        openapi.SchemaTypeInteger,
					Format:      openapi.SchemaFormatInt64,
				},
			},
			{
				Name: "message",
				Schema: &openapi.Schema{
					Description: `A String providing a short description of the error.`,
					Type:        openapi.SchemaTypeString,
				},
			},
			{
				Name: "data",
				Schema: &openapi.Schema{
					Description: `A Primitive or Structured value that contains additional information about the error.`,
					Type:        openapi.SchemaTypeObject,
				},
			},
		},
	}

	if data != nil {
		res.Properties[2].Schema = p.generateSwaggerArgument(data).GetReferenceObject()
	}

	return res
}

func (p *Printer) generateSwaggerArgumentRequestBody(params *model.Argument, methodName string) *openapi.Schema {
	return &openapi.Schema{
		Title: fmt.Sprintf("Request body of the %s method", methodName),
		Type:  openapi.SchemaTypeObject,
		Properties: openapi.SchemaObject{
			{
				Name: "jsonrpc",
				Schema: &openapi.Schema{
					Description: `A String specifying the version of the JSON-RPC protocol. MUST be exactly "2.0".`,
					Type:        openapi.SchemaTypeString,
					Enum:        []interface{}{"2.0"},
				},
			},
			{
				Name: "id",
				Schema: &openapi.Schema{
					Description: `An identifier established by the Client.`,
					Type:        openapi.SchemaTypeString,
					Format:      openapi.SchemaFormat("uuid"),
				},
			},
			{
				Name: "method",
				Schema: &openapi.Schema{
					Description: `A String containing the name of the method to be invoked.`,
					Type:        openapi.SchemaTypeString,
					Enum:        []interface{}{methodName},
				},
			},
			{
				Name:   "params",
				Schema: p.generateSwaggerArgument(params).GetReferenceObject(),
			},
		},
	}
}

func (p *Printer) generateSwaggerArgumentResponseBody(result *model.Argument, methodName string, errType *openapi.Schema) *openapi.Schema {
	return &openapi.Schema{
		Title: fmt.Sprintf("Response body of the %s method", methodName),
		Type:  openapi.SchemaTypeObject,
		Properties: openapi.SchemaObject{
			{
				Name: "jsonrpc",
				Schema: &openapi.Schema{
					Description: `A String specifying the version of the JSON-RPC protocol. MUST be exactly "2.0".`,
					Type:        openapi.SchemaTypeString,
					Enum:        []interface{}{"2.0"},
				},
			},
			{
				Name: "id",
				Schema: &openapi.Schema{
					Description: `It MUST be the same as the value of the id member in the Request.`,
					Type:        openapi.SchemaTypeString,
					Format:      openapi.SchemaFormat("uuid"),
				},
			},
			{
				Name:   "error",
				Schema: errType,
			},
			{
				Name:   "result",
				Schema: p.generateSwaggerArgument(result).GetReferenceObject(),
			},
		},
	}
}
