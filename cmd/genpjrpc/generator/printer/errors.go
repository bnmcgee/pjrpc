package printer

import (
	"errors"
)

// ErrInvalidData is generic error returns when printer got unexpected data.
var ErrInvalidData = errors.New("invalid data")
