module gitlab.com/pjrpc/pjrpc/cmd/genpjrpc

go 1.20

require (
	github.com/iancoleman/strcase v0.2.0
	github.com/rs/zerolog v1.29.0
	github.com/stretchr/testify v1.8.1
	gitlab.com/so_literate/fconfig v0.0.5
	gitlab.com/so_literate/gentools v0.0.4
	gitlab.com/so_literate/gentools/logger v0.0.1
	gitlab.com/so_literate/gentools/packager v0.0.6
	gitlab.com/so_literate/graceful v1.1.0
	gitlab.com/so_literate/openapi v0.0.3
	golang.org/x/tools v0.5.0
)

require (
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/mattn/go-colorable v0.1.12 // indirect
	github.com/mattn/go-isatty v0.0.14 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	gitlab.com/so_literate/object v0.0.1 // indirect
	golang.org/x/mod v0.7.0 // indirect
	golang.org/x/sys v0.4.0 // indirect
	gopkg.in/yaml.v3 v3.0.1 // indirect
)
