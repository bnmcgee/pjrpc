package pjrpc

import (
	"errors"
)

var (
	// ErrPanicInHandler returns in the server when handler called panic.
	ErrPanicInHandler = errors.New("panic in handler")

	// ErrBadStatusCode returns in client when http response has code not 200.
	ErrBadStatusCode = errors.New("bad status code")

	// ErrWrongContentType returns in client when http response has content-type not application/json.
	ErrWrongContentType = errors.New("wrong content type")

	// ErrUnsupportedMods returns in client that are not supported request modifications.
	ErrUnsupportedMods = errors.New("mods are not supported")

	// ErrorResponseChannelClosed returns in async client when response listener got closed channel.
	ErrorResponseChannelClosed = errors.New("response channel is closed")
)
