package pjrpc_test

import (
	"context"
	"encoding/json"
	"errors"
	"reflect"
	"testing"

	"gitlab.com/pjrpc/pjrpc/v2"
	"gitlab.com/pjrpc/pjrpc/v2/storage"
)

var errTest = errors.New("test error")

type ctxKey int

func assertNoError(t *testing.T, err error) {
	t.Helper()

	if err != nil {
		t.Fatalf("unexpected error: %v", err)
	}
}

func assertDeepEqual(t *testing.T, want, got interface{}) {
	t.Helper()

	if !reflect.DeepEqual(want, got) {
		t.Fatalf("not equal:\nwant: %#v\ngot:  %#v", want, got)
	}
}

// TestRouter_Invoke register handlers and invoke.
func TestRouter_Invoke(t *testing.T) {
	t.Parallel()

	const ctxKeyTest ctxKey = 1
	r := pjrpc.NewRouter()

	//
	// Just Invoke.
	//

	methodName := "method_name"
	params := []byte("params")

	testContext := context.WithValue(context.Background(), ctxKeyTest, "keep_context_value")

	res, err := r.Invoke(testContext, methodName, params)
	if res != nil {
		t.Fatal(res)
	}

	if !errors.Is(err, storage.ErrRouteNotFound) {
		t.Fatal(err)
	}

	handler := func(ctx context.Context, p json.RawMessage) (interface{}, error) {
		t.Helper()

		if string(params) != string(p) {
			t.Fatalf("unexpected handler params:\n%q\n%q", string(params), string(p))
		}

		val, ok := ctx.Value(ctxKeyTest).(string)
		if !ok {
			t.Fatal("not ok in handler")
		}

		if val != "keep_context_value" {
			t.Fatalf("unexpected handler ctx value: %q", val)
		}

		return 1, nil
	}

	r.RegisterMethod(methodName, handler)

	res, err = r.Invoke(testContext, methodName, params)
	assertNoError(t, err)
	assertDeepEqual(t, 1, res)

	//
	// Invoke with error.
	//

	methodName = "method_name_2"
	params = []byte("params_2")

	handlerErr := func(_ context.Context, p json.RawMessage) (interface{}, error) {
		if string(params) != string(p) {
			t.Fatalf("unexpected handlerErr params:\n%q\n%q", string(params), string(p))
		}

		return 2, errTest
	}

	res, err = r.Invoke(testContext, methodName, params)
	if res != nil {
		t.Fatal(res)
	}

	if !errors.Is(err, storage.ErrRouteNotFound) {
		t.Fatal(err)
	}

	r.RegisterMethod(methodName, handlerErr)
	res, err = r.Invoke(context.Background(), methodName, params)
	if res != nil {
		t.Fatal(res)
	}

	if !errors.Is(err, errTest) {
		t.Fatal(err)
	}
}

// TestRouter_With testing invoke with router middlewares.
func TestRouter_With(t *testing.T) {
	t.Parallel()

	const (
		ctxKeyTestRouterMW       ctxKey = 10
		ctxKeyTestSecondRouterMW ctxKey = 11
	)
	r := pjrpc.NewRouter()

	//
	// Invoke with single middleware.
	//

	methodName := "method_with_router_wm"
	params := []byte("params")

	handler := func(ctx context.Context, _ json.RawMessage) (interface{}, error) {
		t.Helper()

		val, ok := ctx.Value(ctxKeyTestRouterMW).(string)
		if !ok {
			t.Fatal("not ok in handler")
		}

		if methodName != val {
			t.Fatalf("wrong val in handler: %q != %q", methodName, val)
		}

		return 1, nil
	}

	middleware := func(next pjrpc.Handler) pjrpc.Handler {
		return func(ctx context.Context, p json.RawMessage) (interface{}, error) {
			ctx = context.WithValue(ctx, ctxKeyTestRouterMW, methodName)
			return next(ctx, p)
		}
	}

	r.RegisterMethod(methodName, handler)
	r.With(middleware)

	res, err := r.Invoke(context.Background(), methodName, params)
	assertNoError(t, err)
	assertDeepEqual(t, 1, res)

	//
	// Invoke with two middleware in queue order.
	//
	methodName = "method_with_two_router_wm"

	handler = func(ctx context.Context, _ json.RawMessage) (interface{}, error) {
		t.Helper()

		val, ok := ctx.Value(ctxKeyTestRouterMW).(string)
		if !ok {
			t.Fatal("not ok ctxKeyTestRouterMW in handler 2")
		}

		if methodName == val {
			t.Fatalf("equal in handler 2: %q != %q", methodName, val)
		}

		val, ok = ctx.Value(ctxKeyTestSecondRouterMW).(string)
		if !ok {
			t.Fatal("not ok ctxKeyTestSecondRouterMW in handler 2")
		}

		if val != "second_value" {
			t.Fatalf("not equal in handler 2: `second_value` != %q", val)
		}

		return 2, nil
	}

	middlewareTwo := func(next pjrpc.Handler) pjrpc.Handler {
		return func(ctx context.Context, p json.RawMessage) (interface{}, error) {
			// registered middleware will overwrite this value.
			ctx = context.WithValue(ctx, ctxKeyTestRouterMW, "it_will_be_overwrited")

			ctx = context.WithValue(ctx, ctxKeyTestSecondRouterMW, "second_value")

			return next(ctx, p)
		}
	}

	r.RegisterMethod(methodName, handler)
	r.With(middlewareTwo)

	res, err = r.Invoke(context.Background(), methodName, params)
	assertNoError(t, err)
	assertDeepEqual(t, 2, res)
}

// TestRouter_MethodWith testing invoke with method middlewares.
func TestRouter_MethodWith(t *testing.T) {
	t.Parallel()

	r := pjrpc.NewRouter()
	const (
		ctxKeyTestMethodMW       ctxKey = 20
		ctxKeyTestSecondMethodMW ctxKey = 21
	)

	//
	// Invoke with single middleware.
	//

	methodName := "method_with_method_wm"
	params := []byte("params")

	handler := func(ctx context.Context, _ json.RawMessage) (interface{}, error) {
		t.Helper()

		val, ok := ctx.Value(ctxKeyTestMethodMW).(string)
		if !ok {
			t.Fatal("not ok in handler")
		}

		if methodName != val {
			t.Fatalf("wrong val in handler: %q != %q", methodName, val)
		}

		return 1, nil
	}

	middleware := func(next pjrpc.Handler) pjrpc.Handler {
		return func(ctx context.Context, p json.RawMessage) (interface{}, error) {
			ctx = context.WithValue(ctx, ctxKeyTestMethodMW, methodName)
			return next(ctx, p)
		}
	}

	err := r.MethodWith(methodName, middleware)
	if !errors.Is(err, storage.ErrRouteNotFound) {
		t.Fatal(err)
	}

	r.RegisterMethod(methodName, handler)

	err = r.MethodWith(methodName, middleware)
	assertNoError(t, err)

	res, err := r.Invoke(context.Background(), methodName, params)
	assertNoError(t, err)
	assertDeepEqual(t, 1, res)

	//
	// Invoke with two middleware in stack order.
	//
	methodName = "method_with_two_method_wm"

	handler = func(ctx context.Context, _ json.RawMessage) (interface{}, error) {
		t.Helper()

		val, ok := ctx.Value(ctxKeyTestMethodMW).(string)
		if !ok {
			t.Fatal("not ok in handler 2")
		}

		if methodName != val {
			t.Fatalf("wrong val in handler 2: %q != %q", methodName, val)
		}

		val, ok = ctx.Value(ctxKeyTestSecondMethodMW).(string)
		if !ok {
			t.Fatal("not ok in handler 2 (ctx 2)")
		}

		if val != "second_value" {
			t.Fatalf("wrong val in handler 2 (ctx 2): 'second_value' != %q", val)
		}

		return 2, nil
	}

	middlewareTwo := func(next pjrpc.Handler) pjrpc.Handler {
		return func(ctx context.Context, p json.RawMessage) (interface{}, error) {
			// registered middleware will overwrite this value.
			ctx = context.WithValue(ctx, ctxKeyTestMethodMW, "it_will_be_overwrited")

			ctx = context.WithValue(ctx, ctxKeyTestSecondMethodMW, "second_value")

			return next(ctx, p)
		}
	}

	r.RegisterMethod(methodName, handler)

	err = r.MethodWith(methodName, middleware)
	assertNoError(t, err)

	err = r.MethodWith(methodName, middlewareTwo)
	assertNoError(t, err)

	res, err = r.Invoke(context.Background(), methodName, params)
	assertNoError(t, err)
	assertDeepEqual(t, 2, res)
}

// TestRouter_WithOrder test order of the middleware.
func TestRouter_WithOrder(t *testing.T) {
	t.Parallel()

	const (
		ctxKeyTestFirstMW  ctxKey = 30
		ctxKeyTestSecondMW ctxKey = 31
	)

	methodName := "method"
	counter := 0

	handler := func(ctx context.Context, _ json.RawMessage) (interface{}, error) {
		t.Helper()

		first, ok := ctx.Value(ctxKeyTestFirstMW).(int)
		if !ok {
			t.Fatal("no ctxKeyTestFirstMW")
		}

		if first != 1 {
			t.Fatal("want 1, got: ", first)
		}

		second, ok := ctx.Value(ctxKeyTestSecondMW).(int)
		if !ok {
			t.Fatal("no ctxKeyTestSecondMW")
		}

		if second != 2 {
			t.Fatal("want 2, got: ", second)
		}

		return second, nil
	}

	firstMW := func(next pjrpc.Handler) pjrpc.Handler {
		return func(ctx context.Context, p json.RawMessage) (interface{}, error) {
			counter++
			return next(context.WithValue(ctx, ctxKeyTestFirstMW, counter), p)
		}
	}

	secondMW := func(next pjrpc.Handler) pjrpc.Handler {
		return func(ctx context.Context, p json.RawMessage) (interface{}, error) {
			counter++
			return next(context.WithValue(ctx, ctxKeyTestSecondMW, counter), p)
		}
	}

	r := pjrpc.NewRouter()
	r.With(firstMW, secondMW)
	r.RegisterMethod(methodName, handler)

	res, err := r.Invoke(context.Background(), methodName, nil)
	assertNoError(t, err)
	assertDeepEqual(t, counter, res)
}
