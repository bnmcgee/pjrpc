package pjrpc

import (
	"bytes"
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"log"
	"runtime"

	"gitlab.com/pjrpc/pjrpc/v2/pjson"
	"gitlab.com/pjrpc/pjrpc/v2/storage"
)

var _ Registrator = &Server{}

// Server as an protocol agnostic only JSON-RPC specification.
type Server struct {
	// Contains handlers for JSON-RPC methods.
	Router *Router

	// Logger is an optional logger as a last chance to alert about error.
	// Also writes panics from handlers.
	Logger *log.Logger

	// Panic handler calls when your rpc handler make panic.
	// There will be default panic handler (DefaultRestoreOnPanic).
	OnPanic func(ctx context.Context, err error) *ErrorResponse

	// OnErrorParseRequest handler calls when server can't parse client request.
	// There will be default handler (DefaultOnErrorParseRequest).
	OnErrorParseRequest func(ctx context.Context, err error) *ErrorResponse
}

// NewServer returns new protocol agnostic JSON-RPC server.
func NewServer() *Server {
	s := &Server{
		Router: NewRouter(),
		Logger: nil,

		OnPanic:             nil,
		OnErrorParseRequest: nil,
	}

	s.OnPanic = s.DefaultRestoreOnPanic
	s.OnErrorParseRequest = s.DefaultOnErrorParseRequest

	return s
}

func (s *Server) logError(place, message string, err error) {
	if s.Logger == nil {
		return
	}

	s.Logger.Printf("(%s): %s: %s\n", place, message, err)
}

// SetLogger sets your io.Writer as an error logger of the pjrpc server.
// Also you can set your own *log.Logger in Logger field.
func (s *Server) SetLogger(w io.Writer) {
	s.Logger = log.New(w, "[pjrpc-server] ", 0)
}

// DefaultRestoreOnPanic default panic handler.
// It just prints error in log and sets default internal error in response.
func (s *Server) DefaultRestoreOnPanic(_ context.Context, err error) *ErrorResponse {
	s.logError("DefaultRestoreOnPanic", "panic", err)
	return JRPCErrInternalError()
}

// DefaultOnErrorParseRequest default handler calls when server can't parse client request.
// Returns JSON-RPC error Invalid Request with static text.
func (*Server) DefaultOnErrorParseRequest(_ context.Context, _ error) *ErrorResponse {
	return JRPCErrInvalidRequest("failed to parse request")
}

// Serve is a main method of the Server. It parses body of the request and builds response.
// This method never returns error but puts it in the response.
func (s *Server) Serve(ctx context.Context, body json.RawMessage) json.RawMessage {
	res, err := s.parseRequest(ctx, body)
	if err != nil {
		return res.buildError(err)
	}

	if len(res.requests) == 0 {
		return res.buildError(JRPCErrInvalidRequest())
	}

	res.responses = make(BatchResponses, 0, len(res.requests))

	for _, request := range res.requests {
		s.invokeMethod(ctx, res, request)
	}

	return res.buildMessage()
}

// RegisterMethod saves handler of the method to starage.
func (s *Server) RegisterMethod(methodName string, h Handler) {
	s.Router.RegisterMethod(methodName, h)
}

// With adds middlewares to the router's queue of the middlewares.
func (s *Server) With(mws ...Middleware) {
	s.Router.With(mws...)
}

func convertError(err error) *ErrorResponse {
	jrpcErr := &ErrorResponse{}
	if errors.As(err, &jrpcErr) {
		return jrpcErr
	}

	if errors.Is(err, storage.ErrRouteNotFound) {
		return JRPCErrMethodNotFound()
	}

	return JRPCErrServerError(-32000, err.Error())
}

func isRequestValid(req *Request) error {
	if req.JSONRPC != JSONRPCVersion {
		return JRPCErrInvalidRequest("wrong value of the field 'jsonrpc'")
	}

	return nil
}

type serverResult struct {
	requests  BatchRequests
	responses BatchResponses
	isBatch   bool
}

func (sr *serverResult) buildError(err error) json.RawMessage {
	var (
		srcBody interface{}
		resp    = NewResponseFromError(err)
	)

	if sr != nil && sr.isBatch {
		srcBody = BatchResponses{resp}
	} else {
		srcBody = resp
	}

	body, _ := pjson.Marshal(srcBody) //nolint:errcheck // You can't catch error with regular struct.

	return body
}

func (sr *serverResult) buildMessage() json.RawMessage {
	if len(sr.responses) == 0 {
		return nil
	}

	var body []byte

	if sr.isBatch {
		body, _ = pjson.Marshal(sr.responses) //nolint:errcheck // You can't catch error with regular struct.
	} else {
		body = sr.responses[0].JSON()
	}

	return body
}

func parseSingleRequest(res *serverResult, decoder *pjson.Decoder) error {
	req := Request{}

	err := decoder.Decode(&req)
	if err != nil {
		return fmt.Errorf("decoder.Decode: %w", err)
	}

	res.requests = BatchRequests{&req}

	return nil
}

func parseBatchRequest(res *serverResult, decoder *pjson.Decoder) error {
	err := decoder.Decode(&res.requests)
	if err != nil {
		return fmt.Errorf("decoder.Decode: %w", err)
	}

	return nil
}

func (s *Server) restoreOnPanic(ctx context.Context, rec interface{}, method string) *ErrorResponse {
	buf := make([]byte, 65536) // 64 << 10.
	buf = buf[:runtime.Stack(buf, false)]

	err := fmt.Errorf("%w: '%s': %v\n%s", ErrPanicInHandler, method, rec, buf)

	if s.OnPanic != nil {
		return s.OnPanic(ctx, err)
	}

	s.logError("restorePanic", "panic", err)

	if s.OnPanic == nil && s.Logger == nil {
		log.Println(err.Error())
	}

	return JRPCErrInternalError()
}

func (s *Server) invokeMethod(ctx context.Context, res *serverResult, request *Request) {
	err := isRequestValid(request)
	if err != nil {
		res.responses = append(res.responses, NewResponseFromRequest(request).SetError(err))
		return
	}

	ctxData, ok := ContextGetData(ctx)
	if !ok {
		ctxData = new(ContextData)
		ctx = ContextSetData(ctx, ctxData)
	}

	ctxData.JRPCRequest = request
	ctxData.IsBatch = res.isBatch

	defer func() {
		if rec := recover(); rec != nil {
			resp := NewResponseFromRequest(request)
			resp.Error = s.restoreOnPanic(ctx, rec, request.Method)

			res.responses = append(res.responses, resp)
		}
	}()

	result, err := s.Router.Invoke(ctx, request.Method, request.Params)
	if request.IsNotification() { // No response on notification.
		return
	}

	if err != nil {
		res.responses = append(res.responses, NewResponseFromRequest(request).SetError(err))
		return
	}

	res.responses = append(res.responses, NewResponseFromRequest(request).SetResult(result))
}

func (s *Server) parseRequest(ctx context.Context, rawBody json.RawMessage) (*serverResult, error) {
	message := bytes.NewBuffer(rawBody)

	if message.Len() == 0 {
		return nil, JRPCErrInvalidRequest("message length is 0")
	}

	firstSymbol, _, _ := message.ReadRune() //nolint:errcheck // Impossible error on non empty message.
	_ = message.UnreadRune()                //nolint:errcheck // Impossible error after successful read rune.

	res := &serverResult{
		requests:  nil,
		responses: nil,
		isBatch:   firstSymbol == '[',
	}

	decoder := pjson.NewDecoder(message)

	var err error

	if res.isBatch {
		err = parseBatchRequest(res, decoder)
	} else {
		err = parseSingleRequest(res, decoder)
	}

	if err != nil {
		return res, s.OnErrorParseRequest(ctx, err)
	}

	return res, nil
}
