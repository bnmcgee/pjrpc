package pjrpc_test

import (
	"context"
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"

	"gitlab.com/pjrpc/pjrpc/v2"
)

type badReaderCloser struct{}

func (*badReaderCloser) Read(_ []byte) (n int, err error) {
	return 0, errTest
}

func (*badReaderCloser) Close() error {
	return errTest
}

func createRequest(t *testing.T, body string) (*httptest.ResponseRecorder, *http.Request) {
	t.Helper()

	req, err := http.NewRequestWithContext(
		context.Background(),
		http.MethodPost,
		"/rpc",
		strings.NewReader(body),
	)
	if err != nil {
		t.Fatalf("failed to create http request")
	}

	req.Header.Set("Content-Type", "application/json")

	rr := httptest.NewRecorder()

	return rr, req
}

func TestServerHTTP(t *testing.T) {
	t.Parallel()

	srv := pjrpc.NewServerHTTP()

	testCase := func(rr *httptest.ResponseRecorder, req *http.Request, want string) {
		t.Helper()

		srv.ServeHTTP(rr, req)
		if rr.Code != http.StatusOK {
			t.Fatalf("wrong status code: %d", rr.Code)
		}

		gotCT := rr.Header().Get(pjrpc.ContentTypeHeaderName)
		if pjrpc.ContentTypeHeaderValue != gotCT {
			t.Fatal("wrong Content-Type header value:", gotCT)
		}

		if rr.Body.String() != want {
			t.Fatalf("wrong body:\nwant: %s\ngot:  %s", want, rr.Body.String())
		}
	}

	srv.RegisterMethod("method", func(ctx context.Context, params json.RawMessage) (interface{}, error) {
		if string(params) != `{"field":"value"}` {
			return "wrong params", nil
		}

		return "ok", nil
	})

	rr, req := createRequest(t, `{"jsonrpc":"2.0","id":"1","method":"method","params":{"field":"value"}}`)
	testCase(rr, req, `{"jsonrpc":"2.0","id":"1","result":"ok"}`)

	rr, req = createRequest(t, `{"jsonrpc":"2.0","id":"2","method":"method","params":"wrong params"}`)
	testCase(rr, req, `{"jsonrpc":"2.0","id":"2","result":"wrong params"}`)

	rr, req = createRequest(t, `{"jsonrpc":"2.0","id":"3","method":"foo_bar","params":"wrong params"}`)
	testCase(rr, req, `{"jsonrpc":"2.0","id":"3","error":{"code":-32601,"message":"Method not found"}}`)

	rr, req = createRequest(t, `{"jsonrpc":"2.0","method":"method"}`)
	testCase(rr, req, ``)

	rr, req = createRequest(t, `{"jsonrpc":"2.0","method":"method"}`)
	req.Header.Del(pjrpc.ContentTypeHeaderName)
	testCase(rr, req, `{"jsonrpc":"2.0","error":{"code":-32600,"message":"Invalid Request","data":"content-type must be 'application/json'"}}`)

	rr, req = createRequest(t, `{"jsonrpc":"2.0","id":"4","method":"method","params":{"field":"value"}}`)
	req.Header.Set(pjrpc.ContentTypeHeaderName, "application/json;charset=UTF-8")
	testCase(rr, req, `{"jsonrpc":"2.0","id":"4","result":"ok"}`)

	rr, req = createRequest(t, `{"jsonrpc":"2.0","id":"1","method":"method","params":{}}`)
	req.Body = &badReaderCloser{}
	testCase(rr, req, `{"jsonrpc":"2.0","error":{"code":-32700,"message":"Parse error","data":"failed to read body"}}`)
}
