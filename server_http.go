package pjrpc

import (
	"encoding/json"
	"io"
	"net/http"
	"strings"
)

var (
	_ http.Handler = &ServerHTTP{}
	_ Registrator  = &ServerHTTP{}
)

// ServerHTTP JSON-RPC server over HTTP protocol.
type ServerHTTP struct {
	*Server
}

// NewServerHTTP creates new server with default error handlers and empty router.
func NewServerHTTP() *ServerHTTP {
	return &ServerHTTP{Server: NewServer()}
}

// ServeHTTP implements interface of handler in http package.
func (s *ServerHTTP) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	defer r.Body.Close() //nolint:errcheck // It is not important error.

	if !strings.Contains(r.Header.Get(ContentTypeHeaderName), ContentTypeHeaderValue) {
		s.sendMessage(w, NewResponseFromError(JRPCErrInvalidRequest("content-type must be 'application/json'")).JSON())
		return
	}

	body, err := io.ReadAll(r.Body)
	if err != nil {
		s.sendMessage(w, NewResponseFromError(JRPCErrParseError("failed to read body")).JSON())
		return
	}

	ctx := ContextSetData(r.Context(), &ContextData{HTTTRequest: r})

	resp := s.Serve(ctx, body)

	s.sendMessage(w, resp)
}

func (s *ServerHTTP) sendMessage(w http.ResponseWriter, body json.RawMessage) {
	w.Header().Set(ContentTypeHeaderName, ContentTypeHeaderValue)
	w.WriteHeader(http.StatusOK)

	_, _ = w.Write(body) //nolint:errcheck // It's client side problem don't care about it.
}
