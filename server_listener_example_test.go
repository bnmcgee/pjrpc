package pjrpc_test

import (
	"context"
	"encoding/json"
	"fmt"
	"net"

	"gitlab.com/pjrpc/pjrpc/v2"
	"gitlab.com/pjrpc/pjrpc/v2/client"
)

func ExampleServerListener() {
	listner, err := net.Listen("tcp", ":35396")
	if err != nil {
		fmt.Println("net.Listen:", err)
		return
	}

	srv := pjrpc.NewServerListener(listner)

	srv.RegisterMethod("method", func(ctx context.Context, params json.RawMessage) (interface{}, error) {
		fmt.Println("request:", string(params))
		return "response", nil
	})

	go func() {
		if errListen := srv.Listen(); errListen != nil {
			fmt.Println("srv.Listen:", errListen)
		}
	}()

	conn, err := net.Dial("tcp", ":35396")
	if err != nil {
		fmt.Println("net.Dial:", err)
		return
	}

	cl := client.NewAsyncClient(conn)

	go func() {
		if errListen := cl.Listen(); errListen != nil {
			fmt.Println("cl.Listen:", errListen)
		}
	}()

	var res string
	err = cl.Invoke(context.Background(), "1", "method", "params", &res)
	if err != nil {
		fmt.Println("cl.Invoke:", err)
		return
	}

	if err = cl.Close(); err != nil {
		fmt.Println("cl.Close:", err)
		return
	}

	if err = srv.Close(); err != nil {
		fmt.Println("cl.Close:", err)
		return
	}

	fmt.Println("response:", res)

	// Output:
	//
	// request: "params"
	// response: response
}
