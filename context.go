package pjrpc

import (
	"context"
	"net"
	"net/http"
)

type ctxKey int

const (
	ctxKeyData ctxKey = iota + 1
	ctxKeyConnection
)

// ContextData router data of the request as context.
type ContextData struct {
	// It's raw HTTP request from router.
	HTTTRequest *http.Request
	// It's parsed body of the request.
	JRPCRequest *Request
	// It will be true if this JSON-RPC request is a part of the one request.
	IsBatch bool
}

// ContextSetData sets ContextData to the context.
func ContextSetData(ctx context.Context, data *ContextData) context.Context {
	return context.WithValue(ctx, ctxKeyData, data)
}

// ContextGetData returns ContextData from the context.
func ContextGetData(ctx context.Context) (d *ContextData, ok bool) {
	d, ok = ctx.Value(ctxKeyData).(*ContextData)
	return d, ok
}

// ContextGetConnection returns websocket connection from context.
func ContextGetConnection(ctx context.Context) (conn net.Conn, ok bool) {
	conn, ok = ctx.Value(ctxKeyConnection).(net.Conn)
	return conn, ok
}

// ContextSetConnection sets websocket connection to the context.
func ContextSetConnection(ctx context.Context, conn net.Conn) context.Context {
	return context.WithValue(ctx, ctxKeyConnection, conn)
}
