module without-code-gen

go 1.17

require gitlab.com/pjrpc/pjrpc/v2 v2.0.0

replace gitlab.com/pjrpc/pjrpc/v2 => ../../
