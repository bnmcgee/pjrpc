module websocket

go 1.17

replace (
	gitlab.com/pjrpc/pjrpc/v2 => ../../
	gitlab.com/pjrpc/pjrpc/ws => ../../ws
)

require (
	gitlab.com/pjrpc/pjrpc/v2 v2.0.0
	gitlab.com/pjrpc/pjrpc/ws v1.0.0
	golang.org/x/net v0.5.0
)
