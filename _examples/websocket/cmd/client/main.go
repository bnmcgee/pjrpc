package main

import (
	"log"
	"time"

	"golang.org/x/net/websocket"
)

// It is just custom websocket client.
func main() {
	conn, err := websocket.Dial("ws://127.0.0.1:8081/ws", "", "http://127.0.0.1:8081")
	if err != nil {
		log.Fatal("websocket.Dial:", err)
	}

	defer conn.Close()
	go listen(conn)

	msg := `{"jsonrpc":"2.0","id":"1","method":"hello","params":{"name":"Client"}}`

	for {
		time.Sleep(time.Second * 3)

		err = websocket.Message.Send(conn, msg)
		if err != nil {
			log.Fatal("websocket.Send:", err)
		}
	}
}

func listen(conn *websocket.Conn) {
	for {
		var msg string
		err := websocket.Message.Receive(conn, &msg)
		if err != nil {
			log.Fatal("websocket.Receive:", err)
		}

		log.Println("got message:", msg)
	}
}
