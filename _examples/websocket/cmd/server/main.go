package main

import (
	"context"
	"fmt"
	"log"
	"net"
	"net/http"
	"sync"

	"gitlab.com/pjrpc/pjrpc/v2"
	"gitlab.com/pjrpc/pjrpc/v2/client"
	"gitlab.com/pjrpc/pjrpc/ws"
	"golang.org/x/net/websocket"

	"websocket/model"
	"websocket/model/rpcclient"
	"websocket/model/rpcserver"
)

func main() {
	conf, err := websocket.NewConfig("ws://127.0.0.1:8081/ws", "http://127.0.0.1:8081")
	if err != nil {
		log.Fatalln("websocket.NewConfig:", err)
	}

	svc := &service{pool: make(map[net.Conn]rpcclient.NotificatorServerClient)}

	wsConf := ws.Config{
		WSConfig:  conf,
		Handshake: handshake,
	}

	wsServ := ws.NewServerWebsocket(wsConf)

	wsServ.OnNewConnection = func(conn net.Conn) (accept bool) {
		log.Println("new connect:", conn.RemoteAddr())
		svc.addClient(conn)
		return true
	}

	wsServ.OnCloseConnection = func(conn net.Conn) {
		log.Println("close connect:", conn.RemoteAddr())
		svc.removeClient(conn)
	}

	wsServ.OnErrorReceive = func(conn net.Conn, err error) (needClose bool) {
		log.Println("error in receive:", conn.RemoteAddr(), err.Error())
		return true
	}

	wsServ.OnErrorSend = func(conn net.Conn, err error) (needClose bool) {
		log.Println("error in send:", conn.RemoteAddr(), err.Error())
		return true
	}

	rpcserver.RegisterClassicServerServer(wsServ, svc)

	srv := http.NewServeMux()
	srv.Handle("/ws", wsServ)

	log.Println("started at: 8081")

	err = http.ListenAndServe("127.0.0.1:8081", srv)
	if err != nil {
		log.Fatalln("http.ListenAndServe:", err)
	}
}

func handshake(conf *websocket.Config, req *http.Request) error {
	log.Println("handshake:", req.RemoteAddr)
	return nil
}

type service struct {
	mu   sync.RWMutex
	pool map[net.Conn]rpcclient.NotificatorServerClient
}

func (s *service) addClient(conn net.Conn) {
	cl := rpcclient.NewNotificatorServerClient(client.NewAsyncClient(conn))

	s.mu.Lock()
	s.pool[conn] = cl
	s.mu.Unlock()
}

func (s *service) removeClient(conn net.Conn) {
	s.mu.Lock()
	delete(s.pool, conn)
	s.mu.Unlock()
}

// Hello sends Notification to all our clients except the client of this request.
func (s *service) Hello(ctx context.Context, in *model.HelloReqResp) (*model.HelloReqResp, error) {
	conn, _ := pjrpc.ContextGetConnection(ctx)

	// If you want to know a real remote address of the connection you need to convert it into *websocket.Conn.
	remoteAddr := conn.(*websocket.Conn).Request().RemoteAddr

	log.Println("got message from:", in.Name, remoteAddr)

	notify := &model.NotifyReq{
		Message: fmt.Sprintf("I have got 'hello' request from: %s (%s)", in.Name, remoteAddr),
	}

	s.mu.RLock()

	for c, cl := range s.pool {
		if conn == c {
			continue // Do not send notify to the same client.
		}

		err := cl.Notify(ctx, notify)
		if err != nil {
			log.Println("can't send notify to:", remoteAddr)
		}
	}

	s.mu.RUnlock()

	return &model.HelloReqResp{Name: "Server"}, nil
}
