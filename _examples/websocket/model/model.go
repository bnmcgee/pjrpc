package model

type HelloReqResp struct {
	Name string `json:"name"` // Your name.
}

type NotifyReq struct {
	Message string `json:"message"` // Message of the notification.
}

// ClassicServer just an JSON-RPC serivce with server router.
//
//go:generate genpjrpc -search.name=ClassicServer -print.place.path_client=""
type ClassicServer interface {
	// Hello a classic method with request and response.
	Hello(HelloReqResp) HelloReqResp
}

// NotificatorServer it is description of the notification protocol from server to client.
//
//go:generate genpjrpc -search.name=NotificatorServer -print.place.path_server=""
type NotificatorServer interface {
	// Notify a method without result.
	Notify(NotifyReq)
}
