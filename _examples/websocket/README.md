# Websocket

It's websocket server with logic like a chat.


Package `model` contains 2 services:
- Classic - just regular request/response service
- Notificator - server notification for clients

Flow:
- Server gets a new websocket connection.
- Registers connection in pool as "Client"
- Waits request "Hello" from the Client
- On each request server sends notification to the other clients


```
===========================================

 Client ---- open a connection ---> Server
                                      |
                put the connection in |
                                      v
                                    [Pool]

===========================================

 Client ---- send request Hello --> Server
                                      |
        send notify to all clients in |
                                      v
 Client <--- send notify ---------- [Pool]
                                      |
                    prepared response |
                                      V
 Client <--- send response ----- [req conn]

===========================================

 Client -- close the connection --> Server
                                      |
           remove the connection from |
                                      V
                                    [Pool]

===========================================
```

## Run

### Run the server:

`cd cmd/server && go run ./`

### Run the 2 copies of clients:

`cd cmd/client && go run ./`

For each Hello request other clients will get Notification about it.
