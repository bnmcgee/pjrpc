package service

type In struct {
	Field string
}

type Out string

//go:generate genpjrpc -search.name=Service
type Service interface {
	Method(In) Out
	Exclusive(In) Out
}
