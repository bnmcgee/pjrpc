package main

import (
	"context"
	"log"
	"net/http"
	"net/http/httputil"

	"gitlab.com/pjrpc/pjrpc/v2/client"

	"middlewares/service"
	"middlewares/service/rpcclient"
)

// registered in client.New and will be called before each request.
func modRequestDebbuger(req *http.Request) {
	dump, err := httputil.DumpRequestOut(req, true)
	if err != nil {
		log.Println("failed to DumpRequestOut:", err)
	}

	log.Println("request: ", string(dump))
}

func main() {
	cl, err := client.New("http://127.0.0.1:8080/rpc/", modRequestDebbuger)
	if err != nil {
		log.Fatalf("failed to client.New: %s", err)
	}

	rpc := rpcclient.NewServiceClient(cl)
	ctx := context.Background()

	in := &service.In{Field: "without exclusive mod"}

	out, err := rpc.Method(ctx, in)
	if err != nil {
		log.Fatalln("rpc.Method:", err)
	}

	log.Println("response Method:", out)

	in.Field = "with exclusive mod"
	out, err = rpc.Exclusive(ctx, in, client.ModWithHeader("X-Header-Exclusive", "Value"))
	if err != nil {
		log.Fatalln("rpc.Method:", err)
	}

	log.Println("response Exclusive:", out)
}
