JSON-RPC over TCP
========

This is an example of using the generic ServerListener with the TCP protocol.

This example shows how you can authorize and validate a connection.

Run the server:
```sh
cd cmd/server && go run ./
```

Run the client:
```sh
cd cmd/client && go run ./
```
