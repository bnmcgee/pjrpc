package model

import (
	"time"
)

type AuthReq struct {
	Login    string
	Password string
}

type AuthResp struct{}

type Notify struct {
	When time.Time
}

//go:generate genpjrpc -search.name=Service
type Service interface {
	// Auth takes login and password and returns empty success message.
	Auth(AuthReq) AuthResp

	// Notify protected notification for authorised connections.
	Notify(Notify)
}
