module tcp

go 1.17

replace gitlab.com/pjrpc/pjrpc/v2 => ../../

require (
	github.com/google/uuid v1.3.0
	gitlab.com/pjrpc/pjrpc/v2 v2.0.0
)
