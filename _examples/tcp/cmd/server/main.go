package main

import (
	"context"
	"log"
	"net"
	"sync"

	"gitlab.com/pjrpc/pjrpc/v2"

	"tcp/model"
	"tcp/model/rpcserver"
)

func main() {
	listener, err := net.Listen("tcp", "127.0.0.1:7595")
	if err != nil {
		log.Fatalln("net.Listen:", err)
	}

	srv := pjrpc.NewServerListener(listener)

	defer srv.Close()

	svc := &service{authorisedConns: new(sync.Map)}

	rpcserver.RegisterServiceServer(srv, svc)

	srv.OnNewConnection = func(conn net.Conn) bool {
		log.Println("got a new connection:", conn.RemoteAddr())
		return true
	}

	srv.OnCloseConnection = func(conn net.Conn) {
		svc.authorisedConns.Delete(conn)
	}

	log.Println("run server")

	err = srv.Listen()
	if err != nil {
		log.Fatalln("srv.Listen:", err)
	}
}

type service struct {
	// I'm just too lazy to use regular map with mutex.
	authorisedConns *sync.Map
}

func (s *service) Auth(ctx context.Context, in *model.AuthReq) (*model.AuthResp, error) {
	if in.Login != "admin" || in.Password != "admin" {
		log.Println("got a hacker!")
		return nil, pjrpc.JRPCErrServerError(401, "wrong creds")
	}

	conn, _ := pjrpc.ContextGetConnection(ctx)

	s.authorisedConns.Store(conn, nil)

	log.Println("Connection is authorised")

	return &model.AuthResp{}, nil
}

func (s *service) Notify(ctx context.Context, in *model.Notify) {
	conn, _ := pjrpc.ContextGetConnection(ctx)

	_, ok := s.authorisedConns.Load(conn)
	if !ok {
		log.Println("got an unauth notify")
		return
	}

	log.Println("Got a Notify:", in.When)
}
