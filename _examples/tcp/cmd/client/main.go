package main

import (
	"context"
	"log"
	"net"
	"time"

	"gitlab.com/pjrpc/pjrpc/v2/client"

	"tcp/model"
	"tcp/model/rpcclient"
)

func main() {
	conn, err := net.Dial("tcp", "127.0.0.1:7595")
	if err != nil {
		log.Fatalln("net.Dial:", err)
	}

	// Server: 'got a new connection: 127.0.0.1:xxxxx'.

	aCl := client.NewAsyncClient(conn)
	defer aCl.Close()

	go func() {
		if err = aCl.Listen(); err != nil {
			log.Fatalln("cl.Listen:", err)
		}
	}()

	cl := rpcclient.NewServiceClient(aCl)

	ctx, cancel := context.WithTimeout(context.Background(), time.Second)
	defer cancel()

	// Try to send notify without authorisation.
	err = cl.Notify(ctx, &model.Notify{})
	if err != nil {
		// We don't get error here because server can't return one on Notification message by protocol.
		log.Fatalln("cl.Notify:", err)
	}

	// Server: 'got an unauth notify'.

	// Try to auth with wrong creds.
	_, err = cl.Auth(ctx, &model.AuthReq{Login: "Not admin", Password: "123"})
	log.Println("Auth protocol error:", err) // Got error about authorisation.

	// Server: 'got a hacker!'.

	// Try to auth with correct creds.
	_, err = cl.Auth(ctx, &model.AuthReq{Login: "admin", Password: "admin"})
	if err != nil {
		log.Fatalln("cl.Auth:", err)
	}

	// Server: 'Connection is authorised'.

	// Try to send notify as a authorisited connection.
	err = cl.Notify(ctx, &model.Notify{When: time.Now()})
	if err != nil {
		log.Fatalln("cl.Notify auth:", err)
	}

	// Server: 'Got a Notify...'

	log.Println("Client sent its notification")
}
