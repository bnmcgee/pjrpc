package main

import (
	"context"
	"log"
	"net/http"

	"gitlab.com/pjrpc/pjrpc/v2"

	"helloworld/model/hello/rpcserver"
	"helloworld/model/types"
)

type rpc struct{}

// Hello responder.
func (r *rpc) HelloMethod(ctx context.Context, in *types.HelloRequest) (*types.HelloResponse, error) {
	log.Printf("got message from: %s, enum: %s", in.Name, in.Option)

	return &types.HelloResponse{Name: "rpc server"}, nil
}

func main() {
	srv := pjrpc.NewServerHTTP()
	srv.SetLogger(log.Writer()) // Server can write body close errors and panics in handlers.

	r := &rpc{}

	rpcserver.RegisterHelloServiceServer(srv, r)

	mux := http.NewServeMux()

	// Be careful about last slash. Requests will be sent with last slash by swagger UI.
	// Recomend just to strip last slash in the router.
	mux.Handle("/rpc/", srv)

	log.Println("Starting rpc server on :8080")

	err := http.ListenAndServe(":8080", mux)
	if err != nil {
		log.Fatalf("http.ListenAndServe: %s", err)
	}
}
