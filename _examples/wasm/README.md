WASM Example
========

# Run

```sh
make run
```

Waiting `Starting rpc server on :8080` log message.

Then open URL [http://127.0.0.1:8080/](http://127.0.0.1:8080/) in browser with F12 debugger.

Switch to `Network` tab and look `/rpc` request.

Also open `Console` tab to find log messages.
