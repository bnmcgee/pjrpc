package service

type In string

type Out string

//go:generate genpjrpc -search.name=Service
type Service interface {
	Hello(In) Out
}
