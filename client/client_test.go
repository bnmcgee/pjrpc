package client_test

import (
	"bytes"
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"net/http"
	"net/url"
	"strings"
	"testing"

	"gitlab.com/pjrpc/pjrpc/v2"
	"gitlab.com/pjrpc/pjrpc/v2/client"
)

type testHTTPClient struct {
	req *http.Request

	resp *http.Response
	err  error
}

func (c *testHTTPClient) Do(req *http.Request) (*http.Response, error) {
	c.req = req
	return c.resp, c.err
}

func noError(t *testing.T, err error) {
	t.Helper()

	if err != nil {
		t.Fatal(err)
	}
}

func TestClientPositive(t *testing.T) {
	t.Parallel()

	ctx := context.Background()
	id := "id_req"
	method := "method_name"
	param := "param string"
	resultType := ""
	result := &resultType

	cl, err := client.New("http://endpoint", client.ModWithBasicAuth("user", "pwd"))
	noError(t, err)

	if cl.URL != "http://endpoint" {
		t.Fatal("cl.URL:", cl.URL)
	}

	if len(cl.Mods) != 1 {
		t.Fatal("wrong numbers of mods:", len(cl.Mods))
	}

	body := bytes.NewBuffer([]byte(`{"jsonrpc":"2.0","id":"1","result":"res string"}`))

	tcl := &testHTTPClient{
		req: nil,
		resp: &http.Response{
			StatusCode:    http.StatusOK,
			Header:        http.Header{pjrpc.ContentTypeHeaderName: []string{pjrpc.ContentTypeHeaderValue}},
			Body:          io.NopCloser(body),
			ContentLength: int64(body.Len()),
		},
		err: nil,
	}

	cl.HTTPClient = tcl
	err = cl.Invoke(ctx, id, method, param, result, client.ModWithHeader("custom", "value"))
	noError(t, err)

	// Check mods.
	user, pwd, ok := tcl.req.BasicAuth()
	if user != "user" {
		t.Fatal("wrong user", user)
	}

	if pwd != "pwd" {
		t.Fatal("wrong pwd", pwd)
	}

	if !ok {
		t.Fatal("not ok")
	}

	if wantHeader := tcl.req.Header.Get("custom"); wantHeader != "value" {
		t.Fatal("wrong custom header:", wantHeader)
	}

	// Result was parsed correctly.
	if *result != "res string" {
		t.Fatal("wrong result:", *result)
	}

	gotBody, err := io.ReadAll(tcl.req.Body)
	noError(t, err)

	wantBody := `{"jsonrpc":"2.0","id":"id_req","method":"method_name","params":"param string"}`

	if string(gotBody) != wantBody {
		t.Fatalf("wrong body:\n%q\n%q", wantBody, string(gotBody))
	}

	//
	// Method with uninteresting result.
	//
	body = bytes.NewBuffer([]byte(`{"jsonrpc":"2.0","id":"1","result":true}`))
	tcl.resp.Body = io.NopCloser(body)
	err = cl.Invoke(ctx, id, method, param, nil)
	noError(t, err)

	//
	// Error in response.
	//
	body = bytes.NewBuffer([]byte(`{"jsonrpc":"2.0","id":"1","error":{"code":123}}`))
	tcl.resp.Body = io.NopCloser(body)
	err = cl.Invoke(ctx, id, method, param, &result)
	jErr := &pjrpc.ErrorResponse{}

	if !errors.As(err, &jErr) {
		t.Fatal("wrong error", err)
	}

	if jErr.Code != 123 {
		t.Fatal("wrong error code", jErr.Code)
	}

	//
	// Notification.
	//
	tcl.resp.Body = io.NopCloser(nil)
	err = cl.Invoke(ctx, "", method, param, nil)
	noError(t, err)

	//
	// Bad status in the notification request.
	//
	tcl.resp.StatusCode = http.StatusInternalServerError
	err = cl.Invoke(ctx, "", method, param, nil)
	if !errors.Is(err, pjrpc.ErrBadStatusCode) {
		t.Fatal(err)
	}
}

var errDoerErorr = errors.New("doer error")

func asserErrorIs(t *testing.T, got, want error) {
	t.Helper()

	if !errors.Is(got, want) {
		t.Fatalf("%v != %v", got, want)
	}
}

func TestClientNegative(t *testing.T) {
	t.Parallel()

	brokenURL := "http://end`point"
	validURL := "http://endpoint"
	ctx := context.Background()
	id := "id_req"
	method := "method_name"
	resultType := ""
	result := &resultType
	validParam := "valid_param"

	cl, err := client.New(brokenURL)
	urlErr := new(url.Error)
	if !errors.As(err, &urlErr) {
		t.Fatal("wrong error:", err)
	}

	if urlErr.Op != "parse" {
		t.Fatal("wrong err op:", urlErr.Op)
	}

	if cl != nil {
		t.Fatal("not empty client")
	}

	cl, err = client.New(validURL)
	if err != nil {
		t.Fatal("unexpected error:", err)
	}

	if cl == nil {
		t.Fatal("client is nil")
	}

	//
	// can't marshall type.
	//
	invalidParam := make(chan int)
	err = cl.Invoke(ctx, id, method, invalidParam, result)
	jsonErr := &json.UnsupportedTypeError{}
	if !errors.As(err, &jsonErr) {
		t.Fatal(err)
	}

	//
	// can't create request.
	//
	cl.URL = brokenURL
	err = cl.Invoke(ctx, id, method, validParam, result)
	if !errors.As(err, &urlErr) {
		t.Fatal(err)
	}

	//
	// common test client.
	tcl := new(testHTTPClient)
	cl.HTTPClient = tcl
	cl.URL = validURL

	//
	// error in http doer.
	//
	tcl.err = errDoerErorr
	err = cl.Invoke(ctx, id, method, validParam, result)
	asserErrorIs(t, err, errDoerErorr)

	//
	// bad status code.
	//
	tcl.err = nil
	tcl.resp = &http.Response{
		StatusCode: http.StatusBadRequest,
		Body:       io.NopCloser(bytes.NewBuffer(nil)),
	}
	err = cl.Invoke(ctx, id, method, validParam, result)
	asserErrorIs(t, err, pjrpc.ErrBadStatusCode)

	//
	// don't have valid content type.
	//
	tcl.resp.StatusCode = http.StatusOK
	err = cl.Invoke(ctx, id, method, validParam, result)
	asserErrorIs(t, err, pjrpc.ErrWrongContentType)

	//
	// invalid body.
	//
	tcl.resp.Header = http.Header{pjrpc.ContentTypeHeaderName: []string{pjrpc.ContentTypeHeaderValue}}
	err = cl.Invoke(ctx, id, method, validParam, result)
	asserErrorIs(t, err, io.EOF)

	//
	// can't parse to invalid type.
	//
	body := bytes.NewBuffer([]byte(`{"jsonrpc":"2.0","id":"1","result":"res string"}`))
	tcl.resp.Body = io.NopCloser(body)
	wrongResult := ""
	err = cl.Invoke(ctx, id, method, validParam, wrongResult)
	if !strings.Contains(err.Error(), "non-pointer") {
		t.Fatal(err)
	}
}

func TestClient_GeneratedCode(t *testing.T) {
	t.Parallel()

	ctx := context.Background()
	cl, err := client.New("http://localhost")
	noError(t, err)

	tcl := &testHTTPClient{
		resp: &http.Response{
			StatusCode: http.StatusOK,
			Header:     http.Header{pjrpc.ContentTypeHeaderName: []string{pjrpc.ContentTypeHeaderValue}},
		},
	}
	cl.HTTPClient = tcl

	methodName := "method"

	type pointerType struct {
		Field string
	}

	genMethodPointer := func(ctx context.Context, in int, mods ...client.Mod) (result *pointerType, err error) {
		id := "id" // like uuid generator.

		result = new(pointerType)

		err = cl.Invoke(ctx, id, methodName, in, result, mods...)
		if err != nil {
			return result, fmt.Errorf("failed to Invoke method %q: %w", methodName, err)
		}

		return result, nil
	}

	body := bytes.NewBuffer([]byte(`{"jsonrpc":"2.0","id":"id","result":{"Field":"value"}}`))
	tcl.resp.Body = io.NopCloser(body)

	pointerRes, err := genMethodPointer(ctx, 1)
	noError(t, err)

	if pointerRes.Field != "value" {
		t.Fatal(pointerRes)
	}

	type sliceType []string

	genMethodSlice := func(ctx context.Context, in int, mods ...client.Mod) (result sliceType, err error) {
		id := "id" // like uuid generator.

		err = cl.Invoke(ctx, id, methodName, in, &result, mods...)
		if err != nil {
			return result, fmt.Errorf("failed to Invoke method %q: %w", methodName, err)
		}

		return result, nil
	}

	body = bytes.NewBuffer([]byte(`{"jsonrpc":"2.0","id":"id","result":["1"]}`))
	tcl.resp.Body = io.NopCloser(body)

	sliceRes, err := genMethodSlice(ctx, 1)
	noError(t, err)

	if sliceRes[0] != "1" {
		t.Fatal(sliceRes)
	}

	genNotify := func(ctx context.Context, in int, mods ...client.Mod) error {
		err = cl.Invoke(ctx, "", methodName, in, nil, mods...)
		if err != nil {
			return fmt.Errorf("failed to Invoke method %q: %w", methodName, err)
		}

		return nil
	}

	err = genNotify(ctx, 3)
	noError(t, err)
}
