// Package client is a JSON-RPC client.
// It invokes request to endpoint by URL and parses response to custom type.
package client

import (
	"bytes"
	"context"
	"fmt"
	"log"
	"net/http"
	"net/url"
	"strings"

	"gitlab.com/pjrpc/pjrpc/v2"
)

// Invoker client interface for using in generated code.
type Invoker interface {
	Invoke(ctx context.Context, id, method string, params, result interface{}, mods ...Mod) error
}

var _ Invoker = &Client{}

// Mod is a request modificator.
// Mod calls before Doing request.
type Mod func(req *http.Request)

// ModWithBasicAuth adds basic auth to http request.
func ModWithBasicAuth(username, password string) Mod {
	return func(req *http.Request) {
		req.SetBasicAuth(username, password)
	}
}

// ModWithHeader sets your custom header to http request.
func ModWithHeader(header, value string) Mod {
	return func(req *http.Request) {
		req.Header.Set(header, value)
	}
}

// HTTPDoer interface of http client.
type HTTPDoer interface {
	Do(req *http.Request) (*http.Response, error)
}

// Client JSON-RPC client.
// Contains url of the endpoint, http client and request modificators.
type Client struct {
	URL        string
	HTTPClient HTTPDoer
	Mods       []Mod
	Logger     *log.Logger
}

// New creates new JSON-RPC client with default HTTP client.
func New(endpoint string, mods ...Mod) (*Client, error) {
	if _, err := url.Parse(endpoint); err != nil {
		return nil, fmt.Errorf("invalid endpoint url: %w", err)
	}

	cl := &Client{
		URL:        endpoint,
		HTTPClient: new(http.Client),
		Mods:       mods,
	}

	return cl, nil
}

func (c *Client) parseResponse(resp *http.Response) (*pjrpc.Response, error) {
	if resp.StatusCode != http.StatusOK {
		return nil, fmt.Errorf("%w: %d", pjrpc.ErrBadStatusCode, resp.StatusCode)
	}

	ct := resp.Header.Get(pjrpc.ContentTypeHeaderName)

	if !strings.Contains(ct, pjrpc.ContentTypeHeaderValue) {
		return nil, fmt.Errorf("%w: '%s'", pjrpc.ErrWrongContentType, ct)
	}

	jResp, err := pjrpc.NewResponseFromJSONReader(resp.Body)
	if err != nil {
		return nil, fmt.Errorf("pjrpc.NewResponseFromJSONReader: %w", err)
	}

	return jResp, nil
}

func (c *Client) parseNotificationResponse(resp *http.Response) error {
	if resp.StatusCode != http.StatusOK {
		return fmt.Errorf("%w: %d", pjrpc.ErrBadStatusCode, resp.StatusCode)
	}

	return nil
}

// Invoke creates an http request with modifications and executes it.
// Takes request id, JSON-RPC method name, sending params and pointer type to unmarshall result.
// Returns HTTP or JSON-RPC error.
func (c *Client) Invoke(ctx context.Context, id, method string, params, result interface{}, mods ...Mod) error {
	jReq, err := pjrpc.NewRequest(id, method, params)
	if err != nil {
		return fmt.Errorf("pjrpc.NewRequest: %w", err)
	}

	req, err := http.NewRequestWithContext(ctx, http.MethodPost, c.URL, bytes.NewReader(jReq.JSON()))
	if err != nil {
		return fmt.Errorf("create http request: %w", err)
	}

	for _, mod := range mods {
		mod(req)
	}

	for _, mod := range c.Mods {
		mod(req)
	}

	req.Header.Set(pjrpc.ContentTypeHeaderName, pjrpc.ContentTypeHeaderValue)

	httpResp, err := c.HTTPClient.Do(req)

	if httpResp != nil {
		defer httpResp.Body.Close() //nolint:errcheck // This error doesn't matter.
	}

	if err != nil {
		return fmt.Errorf("do http request: %w", err)
	}

	if jReq.IsNotification() {
		err = c.parseNotificationResponse(httpResp)
		if err != nil {
			return fmt.Errorf("parseNotificationResponse: %w", err)
		}

		return nil
	}

	jResp, err := c.parseResponse(httpResp)
	if err != nil {
		return fmt.Errorf("parseResponse: %w", err)
	}

	if jResp.Error != nil {
		return jResp.Error
	}

	if result == nil {
		return nil
	}

	err = jResp.UnmarshalResult(result)
	if err != nil {
		return fmt.Errorf("unmarshal JSON-RPC result: %w", err)
	}

	return nil
}
