package client_test

import (
	"testing"

	"gitlab.com/pjrpc/pjrpc/v2"
	"gitlab.com/pjrpc/pjrpc/v2/client"
)

func TestAsyncResult(t *testing.T) {
	t.Parallel()

	id := "id"

	ars := client.NewAsyncResults()

	ok := ars.SendResult(id, nil)
	if ok {
		t.Fatal("storage must be empty")
	}

	res := ars.CreateResult(id)

	// We can write without reader.
	sendData := &pjrpc.Response{}
	ok = ars.SendResult(id, sendData)
	if !ok {
		t.Fatal("channel must be in storage")
	}

	gotData := <-res

	if gotData != sendData {
		t.Fatal("got wrong data")
	}

	ok = ars.SendResult(id, nil)
	if ok {
		t.Fatal("storage must be empty")
	}

	ars.CreateResult(id)
	ars.DeleteResult(id)
	ok = ars.SendResult(id, sendData)
	if ok {
		t.Fatal("storage must be empty")
	}
}
