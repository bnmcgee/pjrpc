package client

import (
	"bytes"
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"net"

	"gitlab.com/pjrpc/pjrpc/v2"
	"gitlab.com/pjrpc/pjrpc/v2/pjson"
)

var (
	_ Invoker         = &AsyncClient{}
	_ AsyncConnection = net.Conn(nil)
)

// AsyncConnection it is like net.Conn but without unused methods.
type AsyncConnection interface {
	Read(b []byte) (n int, err error)
	Write(b []byte) (n int, err error)
	Close() error
}

// AsyncClient provides client API for notifications or request/response interactions.
type AsyncClient struct {
	Conn    AsyncConnection
	Results *AsyncResults

	// OnParseMessageError is an optional handler that will be called
	// when the listener receives an unmarshable message.
	// The result of the method will be thrown from the Listen method.
	OnParseMessageError func(err error) error

	// OnUnknownResponse is an optional handler that will be called
	// when the listener can't find the request of the incoming response.
	// The result of the method will be thrown from the Listen method.
	OnUnknownResponse func(*pjrpc.Response) error

	// OnRequest is an optional handler that will be called
	// when the listener receives a request e.g. a server sent notification.
	// The result of the method will be thrown from the Listen method.
	OnRequest func(*pjrpc.Request) error

	dec *pjson.Decoder
	enc *pjson.Encoder
}

// NewAsyncClient returns a new async client based on connection.
// Don't forget about method "Listen" if you want to get responses.
// And don't call the method "Listen" if your connection already listened.
func NewAsyncClient(conn AsyncConnection) *AsyncClient {
	return &AsyncClient{
		Conn:    conn,
		Results: NewAsyncResults(),
		dec:     pjson.NewDecoder(conn),
		enc:     pjson.NewEncoder(conn),
	}
}

// Close closes async connection and all response channels.
func (c *AsyncClient) Close() error {
	c.Results.mu.Lock()

	for _, res := range c.Results.data {
		close(res)
	}

	err := c.Conn.Close()

	c.Results.mu.Unlock()

	return err //nolint:wrapcheck // no need to wrap single error here.
}

// Listen runs listener of the connection.
// Use it in a gorutine.
// Don't run this method when you got your connection from server.
func (c *AsyncClient) Listen() error {
	return c.listen()
}

// Invoke sends message over async connection and waited an async response.
// If you want to send Notification you must pass empty id.
// Client Mods doesn't support here.
func (c *AsyncClient) Invoke(ctx context.Context, id, method string, params, result interface{}, mods ...Mod) error {
	if len(mods) > 0 {
		return pjrpc.ErrUnsupportedMods
	}

	req, err := pjrpc.NewRequest(id, method, params)
	if err != nil {
		return fmt.Errorf("pjrpc.NewRequest: %w", err)
	}

	var aRes AsyncResult

	if !req.IsNotification() {
		aRes = c.Results.CreateResult(id)
		defer c.Results.DeleteResult(id)
	}

	err = c.sendMessage(ctx, req)
	if err != nil {
		return fmt.Errorf("sendMessage: %w", err)
	}

	if req.IsNotification() {
		return nil
	}

	var (
		response *pjrpc.Response
		ok       bool
	)

	select {
	case <-ctx.Done():
		return fmt.Errorf("ctx.Done: %w", ctx.Err())
	case response, ok = <-aRes:
	}

	if !ok {
		return pjrpc.ErrorResponseChannelClosed
	}

	if response.Error != nil {
		return response.Error
	}

	if result == nil {
		return nil
	}

	err = response.UnmarshalResult(result)
	if err != nil {
		return fmt.Errorf("response.UnmarshallResult: %w\n%q", err, response.Result)
	}

	return nil
}

func (c *AsyncClient) sendMessage(ctx context.Context, req *pjrpc.Request) error {
	done := make(chan error, 1)

	go func() {
		done <- c.enc.Encode(req)
		close(done)
	}()

	select {
	case <-ctx.Done():
		return fmt.Errorf("ctx.Done: %w", ctx.Err())
	case err := <-done:
		return err
	}
}

func (c *AsyncClient) listen() error {
	var msg json.RawMessage

	methodBytes := []byte("\"method\"")

	for {
		msg = msg[:0]

		err := c.dec.Decode(&msg)
		if err != nil {
			if errors.Is(err, io.EOF) || errors.Is(err, net.ErrClosed) {
				return nil
			}

			return fmt.Errorf("decode: %w", err)
		}

		if bytes.Contains(msg, methodBytes) {
			err = c.processRequest(msg)
			if err != nil {
				return fmt.Errorf("processRequest: %w", err)
			}
		} else {
			err = c.processResponse(msg)
			if err != nil {
				return fmt.Errorf("processResponse: %w", err)
			}
		}
	}
}

func (c *AsyncClient) processResponse(msg json.RawMessage) error {
	resp, err := pjrpc.NewResponseFromJSON(msg)
	if err != nil {
		if c.OnParseMessageError != nil {
			return c.OnParseMessageError(err)
		}

		return nil
	}

	ok := c.Results.SendResult(resp.GetID(), resp)
	if !ok && c.OnUnknownResponse != nil {
		return c.OnUnknownResponse(resp)
	}

	return nil
}

func (c *AsyncClient) processRequest(msg json.RawMessage) error {
	if c.OnRequest == nil {
		// no point continuing if there's no handler registered.
		return nil
	}

	req, err := pjrpc.NewRequestFromJSON(msg)
	if err != nil {
		if c.OnParseMessageError != nil {
			return c.OnParseMessageError(err)
		}
		return nil
	}

	return c.OnRequest(req)
}
