package client_test

import (
	"context"
	"encoding/json"
	"errors"
	"io"
	"strings"
	"testing"
	"time"

	"gitlab.com/pjrpc/pjrpc/v2"
	"gitlab.com/pjrpc/pjrpc/v2/client"
)

type testConnection struct {
	t *testing.T

	wantWrite       string
	sendReadyToRead bool
	sendWriterDone  bool
	writerWait      time.Duration

	readyToRead chan struct{}
	writerDone  chan struct{}
	readerDone  chan struct{}

	reader         *strings.Reader
	sendReaderDone bool

	writeErr error
	readErr  error
}

func (tc *testConnection) Read(b []byte) (n int, err error) {
	<-tc.readyToRead

	if tc.sendReaderDone {
		defer func() {
			tc.readerDone <- struct{}{}
		}()
	}

	if tc.readErr != nil {
		return 0, tc.readErr
	}

	return tc.reader.Read(b)
}

func (tc *testConnection) Write(b []byte) (n int, err error) {
	if tc.wantWrite+"\n" != string(b) {
		tc.t.Fatalf("unexpected write data:\n%q\n%q", tc.wantWrite, b)
	}

	if tc.sendReadyToRead {
		tc.readyToRead <- struct{}{}
	}

	if tc.sendWriterDone {
		tc.writerDone <- struct{}{}
	}

	time.Sleep(tc.writerWait)

	return len(b), tc.writeErr
}

func (tc *testConnection) Close() error {
	tc.writeErr = io.ErrClosedPipe
	tc.readErr = io.ErrClosedPipe

	return nil
}

func TestClientAsync(t *testing.T) {
	t.Parallel()

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*3)
	defer cancel()

	tc := &testConnection{
		t:           t,
		readyToRead: make(chan struct{}, 1),
	}

	cl := client.NewAsyncClient(tc)

	go func() {
		_ = cl.Listen()
	}()

	// Regular using as Request/Response.
	tc.wantWrite = `{"jsonrpc":"2.0","id":"1","method":"method","params":"string as a param"}`
	tc.sendReadyToRead = true
	tc.reader = strings.NewReader(`{"jsonrpc":"2.0","id":"1","result":"string as a result"}`)

	var res string
	err := cl.Invoke(ctx, "1", "method", "string as a param", &res)
	noError(t, err)

	if res != "string as a result" {
		t.Fatal(res)
	}

	// Regular using but igoner a result in the response.
	tc.reader = strings.NewReader(`{"jsonrpc":"2.0","id":"1","result":true}`)

	err = cl.Invoke(ctx, "1", "method", "string as a param", nil)
	noError(t, err)

	// Notification:
	tc.wantWrite = `{"jsonrpc":"2.0","method":"notify","params":null}`
	tc.sendReadyToRead = false

	err = cl.Invoke(ctx, "", "notify", nil, nil)
	noError(t, err)

	// JSON-RPC error in response.
	tc.wantWrite = `{"jsonrpc":"2.0","id":"1","method":"method","params":5}`
	tc.sendReadyToRead = true
	tc.reader = strings.NewReader(`{"jsonrpc":"2.0","id":"1","error":{"code":-32600,"message":"Invalid Request"}}`)

	err = cl.Invoke(ctx, "1", "method", 5, &res)
	jErr := &pjrpc.ErrorResponse{}
	if !errors.As(err, &jErr) && jErr.Code != -32600 {
		t.Fatal(err)
	}

	// Wrong type for response parsing.
	tc.wantWrite = `{"jsonrpc":"2.0","id":"1","method":"method","params":"string as a param"}`
	tc.reader = strings.NewReader(`{"jsonrpc":"2.0","id":"1","result":"string as a result"}`)

	var integer int
	err = cl.Invoke(ctx, "1", "method", "string as a param", &integer)
	jsonErr := &json.UnmarshalTypeError{}
	if !errors.As(err, &jsonErr) {
		t.Fatal(err)
	}
}

func TestClientAsync_Close(t *testing.T) {
	t.Parallel()

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*3)
	defer cancel()

	tc := &testConnection{
		t:          t,
		writerDone: make(chan struct{}, 1),
	}

	cl := client.NewAsyncClient(tc)

	// Close connection while client waits for response.
	invokeErr := make(chan error, 1)

	go func() {
		tc.wantWrite = `{"jsonrpc":"2.0","id":"2","method":"method","params":"params"}`
		tc.sendWriterDone = true
		invokeErr <- cl.Invoke(ctx, "2", "method", "params", nil)
	}()

	<-tc.writerDone

	err := cl.Close()
	noError(t, err)

	err = <-invokeErr
	if !errors.Is(err, pjrpc.ErrorResponseChannelClosed) {
		t.Fatal(err)
	}

	// Try to send request into Closed client.
	err = cl.Invoke(ctx, "2", "method", "params", nil)
	if !errors.Is(err, io.ErrClosedPipe) {
		t.Fatal(err)
	}
}

func TestClientAsync_ContextGames(t *testing.T) {
	t.Parallel()

	tc := &testConnection{
		t:          t,
		writerDone: make(chan struct{}, 1),
	}

	cl := client.NewAsyncClient(tc)

	invokeErr := make(chan error, 1)

	// Imitate long waiting for a response.
	ctx, cancel := context.WithCancel(context.Background())

	go func() {
		tc.wantWrite = `{"jsonrpc":"2.0","id":"3","method":"method","params":"long time"}`
		tc.sendWriterDone = true

		invokeErr <- cl.Invoke(ctx, "3", "method", "long time", nil)
	}()

	<-tc.writerDone
	cancel()

	err := <-invokeErr
	if !errors.Is(err, context.Canceled) {
		t.Fatal(err)
	}

	// Imitate long writer.
	ctx, cancel = context.WithCancel(context.Background())

	go func() {
		tc.wantWrite = `{"jsonrpc":"2.0","id":"3","method":"method","params":"long writer"}`
		tc.writerWait = time.Second
		tc.sendWriterDone = true

		invokeErr <- cl.Invoke(ctx, "3", "method", "long writer", nil)
	}()

	<-tc.writerDone
	cancel()

	err = <-invokeErr
	if !errors.Is(err, context.Canceled) {
		t.Fatal(err)
	}
}

func TestClientAsync_Listen(t *testing.T) {
	t.Parallel()

	tc := &testConnection{
		t:           t,
		readyToRead: make(chan struct{}, 1),
	}

	prototype := client.NewAsyncClient(tc)

	testCase := func(wantErr error, asErr interface{}) {
		t.Helper()

		listenErr := make(chan error, 1)
		cl := client.NewAsyncClient(tc)
		cl.OnParseMessageError = prototype.OnParseMessageError
		cl.OnUnknownResponse = prototype.OnUnknownResponse

		go func() {
			tc.readyToRead <- struct{}{}
			listenErr <- cl.Listen()
		}()

		gotErr := <-listenErr

		if asErr != nil {
			if !errors.As(gotErr, asErr) {
				t.Fatalf("wrong as error:\n%v\n%T", wantErr, asErr)
			}

			return
		}

		if !errors.Is(gotErr, wantErr) {
			t.Fatalf("wrong error:\n%v\n%v", wantErr, gotErr)
		}
	}

	// Server sent EOF (disconnected) it is ok.
	tc.readErr = io.EOF
	testCase(nil, nil)

	// Unexpected error from connection.
	tc.readErr = io.ErrUnexpectedEOF
	testCase(io.ErrUnexpectedEOF, nil)

	// Bad JSON content.
	tc.readErr = nil
	tc.reader = strings.NewReader(`{]`)
	syntaxError := &json.SyntaxError{}
	testCase(nil, &syntaxError)

	// Good JSON but unmarshable for model.
	tc.reader = strings.NewReader(`[]`)
	prototype.OnParseMessageError = func(err error) error {
		return err
	}

	unmarshalTypeError := &json.UnmarshalTypeError{}
	testCase(nil, &unmarshalTypeError)

	// Good JSON-RPC message but unexpected ID.
	tc.reader = strings.NewReader(`{"jsonrpc":"2.0","id":"1","result":{}}`)
	prototype.OnParseMessageError = nil
	prototype.OnUnknownResponse = func(resp *pjrpc.Response) error {
		if resp.GetID() != "1" {
			return io.ErrUnexpectedEOF
		}

		return io.ErrClosedPipe
	}

	testCase(io.ErrClosedPipe, nil)

	// No error when client doesn't have error handlers.
	tc.readerDone = make(chan struct{}, 1)
	tc.sendReaderDone = true

	cl := client.NewAsyncClient(tc)
	listenErr := make(chan error, 1)

	go func() {
		listenErr <- cl.Listen()
	}()

	tc.reader = strings.NewReader(`[]`)
	tc.readyToRead <- struct{}{}
	<-tc.readerDone

	tc.reader = strings.NewReader(`{"jsonrpc":"2.0","id":"1","result":{}}`)
	tc.readyToRead <- struct{}{}
	<-tc.readerDone

	tc.readErr = io.EOF
	tc.readyToRead <- struct{}{}
	<-tc.readerDone

	err := <-listenErr
	noError(t, err)
}

func TestClientAsync_OtherErrors(t *testing.T) {
	t.Parallel()

	ctx := context.Background()
	cl := client.NewAsyncClient(nil)

	// Async client doesn't support http request modifications.
	err := cl.Invoke(ctx, "id", "method", "params", nil, client.ModWithBasicAuth("user", "pass"))
	if !errors.Is(err, pjrpc.ErrUnsupportedMods) {
		t.Fatal(err)
	}

	// Wrong type as params.
	err = cl.Invoke(ctx, "id", "method", make(chan int), nil)
	jsonErr := &json.UnsupportedTypeError{}
	if !errors.As(err, &jsonErr) {
		t.Fatal(err)
	}
}
