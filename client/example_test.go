package client_test

import (
	"context"
	"fmt"
	"net/http"

	"gitlab.com/pjrpc/pjrpc/v2/client"
)

func ExampleClient() {
	cl, err := client.New("http://127.0.0.1:8080/rpc")
	if err != nil {
		return
	}

	type resultType struct {
		Field string `json:"field"`
	}

	var (
		ctx           = context.Background()
		idRequest     = "id_request"
		methodJSONRPC = "method_name"
		params        = "any json.Marshall(v) type"
		result        = resultType{} // It have to be pointer type in the Invoke method.
	)

	err = cl.Invoke(ctx, idRequest, methodJSONRPC, params, &result)
	if err != nil {
		return
	}

	// Output:
}

func ExampleMod() {
	customMod := func(req *http.Request) {
		fmt.Println(req.URL.String())
	}

	// Creates a client with custom mod that prints every request.
	cl, err := client.New("http://127.0.0.1:8080/rpc", customMod)
	if err != nil {
		return
	}

	var (
		ctx           = context.Background()
		idRequest     = "id_request"
		methodJSONRPC = "method_name"
		params        = "params"
		result        = ""
	)

	// Invokes method with an existing mod that sets custom header for this request.
	err = cl.Invoke(ctx, idRequest, methodJSONRPC, params, &result, client.ModWithHeader("X-Header", "value"))
	if err != nil {
		return
	}

	// Output:
	// http://127.0.0.1:8080/rpc
}
